﻿namespace MAS.Work.EmployeeCatalog.Models
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    #endregion

    public class Post
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название должности")]
        public string Name { get; set; }

        [Display(Name = "Работники")]
        public virtual ICollection<Employee> Employees { get; set; }

        public Post()
        { Employees = new List<Employee>(); }
    }

    public class Employee
    {
        public int Id { get; set; }

        [Display(Name = "Информация")]
        public virtual ManInfo ManInfo { get; set; }

        [Required]
        [Display(Name = "Должность")]
        public virtual Post Post { get; set; }

        public virtual ApplicationUser User { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Рабочий отдел")]
        public virtual Departament Departament { get; set; }
    }

    public class ManInfo
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; } = "";

        [Display(Name = "Фамилия")]
        [Required]
        public string SecondName { get; set; }

        [Display(Name = "Возраст")]
        public int Age
        {
            get {
                var age = DateTime.Now.Year - BirthDay.Year;

                if (DateTime.Now.Month < BirthDay.Month || 
                    (DateTime.Now.Month == BirthDay.Month && DateTime.Now.Day < BirthDay.Day))
                    age--;

                return age;
            } 
        }

        [DataType(DataType.Date)]
        [Required]
        [Display(Name = "Дата рождения")]
        public DateTime BirthDay { get; set; }

        public byte[] Photo { get; set; }
        public byte[] PreviewPhoto { get; set; }

        [Display(Name = "ФИО")]
        [NotMapped]
        public string Initals
        {
            get
            {
                var md = string.IsNullOrEmpty(MiddleName) ? "" : MiddleName[0] + "."; // fix zis

                return SecondName + " " + FirstName[0] + ". " + md;
            }
        }

        public ManInfo()
        {
            //Photo = new byte[0];
            //PreviewPhoto = new byte[0];
        }
    }

    public class Organization
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название организации")]
        public string Name { get; set; }

        [Display(Name = "Отделы")]
        public virtual ICollection<Departament> Departaments { get; set; }

        public Organization()
        { Departaments = new List<Departament>(); }
    }

    public class Departament
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название отдела")]
        public string Name { get; set; }

        [Display(Name = "Цели отдела")]
        public string[] Aims { get; set; }

        [Display(Name = "Сотрудники")]
        public virtual ICollection<Employee> Employees { get; set; }

        [Display(Name = "Организация", Description = "Организация, в состав которой входит отдел")]
        public virtual Organization Organization { get; set; }

        public Departament()
        { Employees = new List<Employee>(); }
    }
}