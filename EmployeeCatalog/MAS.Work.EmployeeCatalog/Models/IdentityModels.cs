﻿namespace MAS.Work.EmployeeCatalog.Models
{
    #region References

    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    #endregion

    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Дата регистрации пользователя
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Использует ли пользователь временный пароль, сгенерированный администратором
        /// </summary>
        public bool HasTemporaryPassword { get; set; }

        public ApplicationUser()
            : base()
        {
            RegisterDate = DateTime.Now;
            HasTemporaryPassword = true;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }

}