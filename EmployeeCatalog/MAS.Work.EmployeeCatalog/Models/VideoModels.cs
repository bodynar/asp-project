﻿namespace MAS.Work.EmployeeCatalog.Models {
    #region References

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;

    #endregion

    public class Video {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public TimeSpan Duration { get; set; }

        public bool IsConverted { get; set; }

        public bool IsReady { get; set; }

        public double Rate { get; set; }
        [Required]
        public string Path { get; set; }

        public DateTime UploadingDate { get; set; }
        [Required(AllowEmptyStrings = true)]
        public string Preview { get; set; }

        public virtual ApplicationUser Author { get; set; }

        [ForeignKey("Author")]
        public string AuthorId { get; set; }

        public virtual ApplicationUser Lector { get; set; }

        [ForeignKey("Lector")]
        public string LectorId { get; set; }

        public virtual VideoCategory Category { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public Video() {
            UploadingDate = DateTime.Now;
            Comments = new List<Comment>();
        }

        internal static Video CreateDefault(string Path, ApplicationUser user, VideoCategory cat) =>
            new Video() {
                Description = "Описание не задано",
                Duration = new TimeSpan(0),
                Name = "Название не задано",
                Path = Path,
                AuthorId = user.Id,
                CategoryId = cat.Id,
                Preview = "",
            };
    }

    public class VideoCategory {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class Comment {
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        public virtual ApplicationUser Author { get; set; }
        [ForeignKey("Author")]
        public string AuthorId { get; set; }

        public DateTime Date { get; set; }

        [Required]
        public virtual Video Lecture { get; set; }

        [ForeignKey("Lecture")]
        public int LectureId { get; set; }

        public Comment() {
            Date = DateTime.Today;
        }
    }
}