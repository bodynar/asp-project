﻿namespace MAS.Work.EmployeeCatalog.Mappers
{
    #region References
    using System.Globalization;
    using System.IO;
    using AutoMapper;
    using MAS.Work.EmployeeCatalog.Helpers;
    using MAS.Work.EmployeeCatalog.Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using ViewModels;

    #endregion

    public static partial class AutoMapperConfig
    {
        static void DTOtoViewModels(IMapperConfigurationExpression cfg)
        {
            #region AppUserInfoViewModel

            cfg.CreateMap<ApplicationUser, AppUserInfoViewModel>()
            .ForMember(dest => dest.Register, e => e.MapFrom(src => src.RegisterDate));

            cfg.CreateMap<ManInfo, AppUserInfoViewModel>()
           .ForMember(dest => dest.birthday_, e => e.MapFrom(src => src.BirthDay));

            #endregion

            #region EmployeeViewModel

            cfg.CreateMap<Employee, EmployeeViewModel>()
            .ForMember(dest => dest.FirstName, e => e.MapFrom(src => src.ManInfo.FirstName))
            .ForMember(dest => dest.MiddleName, e => e.MapFrom(src => src.ManInfo.MiddleName))
            .ForMember(dest => dest.SecondName, e => e.MapFrom(src => src.ManInfo.SecondName))
            .ForMember(dest => dest.BirthDay, e => e.MapFrom(src => src.ManInfo.BirthDay))
            .ForMember(dest => dest.Initals, e => e.MapFrom(src => src.ManInfo.Initals))
            .ForMember(dest => dest.PostId, e => e.MapFrom(src => src.Post.Id))
            .ForMember(dest => dest.DepartamentId, e => e.MapFrom(src => src.Departament.Id));


            cfg.CreateMap<Post, EmployeeViewModel>()
            .ForMember(dest => dest.PostId, e => e.MapFrom(src => src.Id));

            cfg.CreateMap<Departament, EmployeeViewModel>()
            .ForMember(dest => dest.DepartamentId, e => e.MapFrom(src => src.Id));

            #endregion

            #region AppUserEditViewModel

            cfg.CreateMap<ApplicationUser, AppUserEditViewModel>()
                 .ForMember(dest => dest.UserName, e => e.MapFrom(src => src.UserName))
                 .ForMember(dest=> dest.UserId, e=> e.MapFrom(src=> src.Id))
                 .ForAllOtherMembers(e => e.Ignore());

            cfg.CreateMap<Employee, AppUserEditViewModel>()
                .ForMember(dest => dest.DepartamentId, e => e.MapFrom(src => src.Departament.Id))
                .ForMember(dest => dest.EmployeeId, e => e.MapFrom(src => src.Id))
                .ForMember(dest => dest.PostId, e => e.MapFrom(src => src.Post.Id))
                .ForMember(dest => dest.BirthDay, e => e.MapFrom(src => src.ManInfo.BirthDay))
                .ForMember(dest => dest.FirstName, e => e.MapFrom(src => src.ManInfo.FirstName))
                .ForMember(dest => dest.SecondName, e => e.MapFrom(src => src.ManInfo.SecondName))
                .ForMember(dest => dest.MiddleName, e => e.MapFrom(src => src.ManInfo.MiddleName))
                .ForMember(dest => dest.Preview, e => e.MapFrom(src => src.ManInfo.PreviewPhoto));

            #endregion

            #region PostViewModel

            cfg.CreateMap<Post, PostViewModel>();

            #endregion

            #region OrganizationViewModel

            cfg.CreateMap<Organization, OrganizationViewModel>();

            #endregion

            #region DepartamentViewModel

            cfg.CreateMap<Departament, DepartamentViewModel>()
                .ForMember(dest => dest.OrganizationId, e => e.MapFrom(src => src.Organization.Id))
                .ForMember(dest => dest.OrgName, e => e.Ignore());

            #endregion

            #region AppUserManageViewModel

            cfg.CreateMap<ApplicationUser, AppUserManageViewModel>();

            #endregion

            #region RoleInfoViewModel

            cfg.CreateMap<IdentityRole, RoleInfoViewModel>()
                .ForMember(dest => dest.Count, e => e.MapFrom(src => src.Users.Count));

            #endregion

            #region RoleEditViewModel

            cfg.CreateMap<IdentityRole, RoleEditViewModel>();

            #endregion

            #region VideoEditViewModel

            cfg.CreateMap<Video, VideoEditViewModel>()
                .ForMember(dest => dest.FileName, e => e.MapFrom(src => new FileInfo(src.Path).Name))
                .ForMember(dest => dest.VideoCategoryId, e => e.MapFrom(src=> src.CategoryId));

            #endregion

            #region VideoPreviewViewModel

            cfg.CreateMap<Video, VideoPreviewViewModel>()
               .ForMember(dest => dest.Author, e => e.MapFrom(src => src.Author.UserName))
               .ForMember(dest => dest.CommentsCount, e => e.MapFrom(src => src.Comments.Count))
               .ForMember(dest => dest.Category, e => e.MapFrom(src => src.Category.Name))
               .ForMember(dest => dest.Lector, 
                          e => e.ResolveUsing(src => src.Lector == null ? 
                                              "Не назначен" 
                                              : src.Lector.UserName))

               .ForMember(dest => dest.PartOfDecription, 
                         e => e.MapFrom(src => src.Description.Length > 100 ? 
                                        src.Description.Substring(0, 100) + ".." 
                                        : src.Description))

               .ForMember(dest => dest.UploadDate, 
                          e => e.MapFrom(src => src.UploadingDate.ToString("d MMMM yyy", CultureInfo.GetCultureInfo("ru-RU"))));

            #endregion

            #region VideoCategoryEditViewModel

            cfg.CreateMap<VideoCategory, VideoCategoryEditViewModel>();

            #endregion

            #region VideoWatchViewModel

            cfg.CreateMap<Video, VideoWatchViewModel>()
                .ForMember(dest=> dest.FileName, e => e.MapFrom(src => new FileInfo(src.Path).Name))
                .ForMember(dest=> dest.Category, e=> e.MapFrom(src=> src.Category.Name))
                .ForMember(dest => dest.Lector,
                           e => e.ResolveUsing(src => src.Lector == null ? "Не назначен" : src.Lector.UserName));

            #endregion

            #region CommentPreviewViewModel

            cfg.CreateMap<Comment, CommentPreviewViewModel>()
                .ForMember(dest => dest.Date, e => e.MapFrom(src => src.Date.ToShortDateString()));

            #endregion
        }
    }
}