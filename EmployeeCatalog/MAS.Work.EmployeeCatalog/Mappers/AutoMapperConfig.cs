﻿namespace MAS.Work.EmployeeCatalog.Mappers
{
    #region References
    using AutoMapper;
    using MAS.Work.EmployeeCatalog.Models;

    #endregion
    public static partial class AutoMapperConfig
    {
        public static void ConfigureMapper()
        {
            Mapper.Initialize(cfg =>
            {
                DTOtoViewModels(cfg);

                ViewModelToDTO(cfg);

                cfg.CreateMap<Departament, Departament>();

                cfg.CreateMap<Organization, Organization>();

                cfg.CreateMap<Post, Post>();

            });

            NLog.LogManager.GetLogger("Debug").Debug("Mapper initialized");
        }
    }
}