﻿namespace MAS.Work.EmployeeCatalog.Mappers
{
    #region References

    using AutoMapper;
    using MAS.Work.EmployeeCatalog.Models;
    using Helpers;
    using Microsoft.AspNet.Identity.EntityFramework;
    using ViewModels;
    #endregion

    public static partial class AutoMapperConfig
    {
        static void ViewModelToDTO(IMapperConfigurationExpression cfg)
        {
            #region ApplicationUser

            cfg.CreateMap<AppUserRegisterViewModel, ApplicationUser>()
                .ForMember(dest => dest.UserName, e => e.MapFrom(src => src.Email.Substring(0, src.Email.IndexOf("@"))
                /*  TranslatinationHelper.CreateUserName(src.SecondName,
                                                       src.FirstName,
                                                       src.MiddleName))*/
                                                       ));

            cfg.CreateMap<EmployeeViewModel, ApplicationUser>()
                .ForMember(dest => dest.Id, e => e.Ignore())
                .ForMember(dest => dest.UserName,
                           e => e.MapFrom(src => TranslatinationHelper.CreateUserName(src.SecondName,
                                                                                      src.FirstName,
                                                                                      src.MiddleName)));

            cfg.CreateMap<AppUserInfoViewModel, ApplicationUser>()
                .ForMember(dest => dest.RegisterDate, e => e.MapFrom(src => src.Register));

            cfg.CreateMap<AppUserEditViewModel, ApplicationUser>()
                .ForMember(dest => dest.UserName, e => e.MapFrom(src => src.UserName))
                .ForAllOtherMembers(e => e.Ignore());
            #endregion

            #region ManInfo

            cfg.CreateMap<EmployeeViewModel, ManInfo>();

            cfg.CreateMap<AppUserRegisterViewModel, ManInfo>()
                .ForMember(dest => dest.MiddleName, e => e.NullSubstitute(""));

            cfg.CreateMap<AppUserEditViewModel, ManInfo>();

            #endregion

            #region Post

            cfg.CreateMap<EmployeeViewModel, Post>()
                .ForMember(dest => dest.Id, e => e.MapFrom(src => src.PostId));

            cfg.CreateMap<PostViewModel, Post>();

            #endregion

            #region Departament

            cfg.CreateMap<EmployeeViewModel, Departament>()
               .ForMember(dest => dest.Id, e => e.MapFrom(src => src.DepartamentId));

            cfg.CreateMap<DepartamentViewModel, Departament>();

            #endregion

            #region Organization

            cfg.CreateMap<OrganizationViewModel, Organization>();

            #endregion

            #region Employee

            cfg.CreateMap<AppUserRegisterViewModel, Employee>()
                .ForMember(dest => dest.ManInfo, e => e.MapFrom(src => src));

            cfg.CreateMap<EmployeeViewModel, Employee>()
                .ForMember(dest => dest.ManInfo, e => e.MapFrom(src => src));

            cfg.CreateMap<AppUserEditViewModel, Employee>()
                .ForMember(dest => dest.ManInfo, e => e.MapFrom(src => src));

            #endregion

            #region IdentityRole

            cfg.CreateMap<RoleCreateViewModel, IdentityRole>();
            cfg.CreateMap<RoleEditViewModel, IdentityRole>();

            #endregion

            #region Video

            cfg.CreateMap<VideoEditViewModel, Video>();
            #endregion

            #region Video Category

            cfg.CreateMap<VideoCategoryEditViewModel, VideoCategory>()
                .ForMember(dest => dest.Id, e => e.Ignore());

            cfg.CreateMap<VideoCategoryCreateViewModel, VideoCategory>()
               .ForMember(dest => dest.Id, e => e.Ignore());

            // TODO: Add mappings
            #endregion

            #region Comment
            cfg.CreateMap<CommentAddViewModel, Comment>(); 
            #endregion

        }
    }
}