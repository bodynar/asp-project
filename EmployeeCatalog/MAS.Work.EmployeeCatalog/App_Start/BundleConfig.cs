﻿namespace MAS.Work.EmployeeCatalog
{
    #region References
    using System.Web.Optimization;
    #endregion
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Scripts

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                        "~/Scripts/bootstrap-datepicker/bootstrap-datepicker*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundlers/growl").Include(
             "~/Scripts/jquery.growl.js"));

            bundles.Add(new ScriptBundle("~/bundlers/bootstrap-validator").Include(
             "~/Scripts/bootstrap-validator/Validator.js"));

            //bundles.Add(new ScriptBundle("~/bundlers/notify").Include(
            //         "~/Scripts/notify.js"));

            #endregion

            #region Styles

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/datepicker").Include(
                      "~/Content/datepicker/bootstrap-datepicker3*"));

            bundles.Add(new StyleBundle("~/Content/growl").Include(
                      "~/Content/jquery.growl.css"));

            bundles.Add(new StyleBundle("~/Content/dropzone").Include(
                      "~/Content/dropzone.css"));

            bundles.Add(new StyleBundle("~/Content/CustomStyle").Include(
                      "~/Content/CustomStyle.css"));

            #endregion
        }
    }
}
