﻿namespace MAS.Work.EmployeeCatalog {
    #region References

    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;
    using Microsoft.Owin.Security;
    using MAS.Work.EmployeeCatalog.Models;
    using System.Net.Mail;
    using Ef;
    using Microsoft.Owin.Security.DataProtection;
    #endregion

    public class EmailService : IIdentityMessageService {
        public async Task SendAsync(IdentityMessage message) {
            using (var client = new SmtpClient())
            {
                var msg = new MailMessage("emp.cat@yandex.ru", message.Destination) {
                    Subject = message.Subject,
                    Body = message.Body,
                    IsBodyHtml = true,
                };

                await client.SendMailAsync(msg);
            }

            // Подключите здесь службу электронной почты для отправки сообщения электронной почты.
            //$return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService {
        public Task SendAsync(IdentityMessage message) {
            // Подключите здесь службу SMS, чтобы отправить текстовое сообщение.
            return Task.FromResult(0);
        }
    }

    // Настройка диспетчера пользователей приложения. UserManager определяется в ASP.NET Identity и используется приложением.
    public class ApplicationUserManager : UserManager<ApplicationUser> {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store) {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<AppDbContext>()));

            // Настройка логики проверки имен пользователей
            manager.UserValidator = new UserValidator<ApplicationUser>(manager) {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
            };

            // Настройка логики проверки паролей
            manager.PasswordValidator = new PasswordValidator {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = false,
                RequireUppercase = false,

            };

            // Настройка параметров блокировки по умолчанию
            manager.UserLockoutEnabledByDefault = false;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 3;

            /* 
            Регистрация поставщиков двухфакторной проверки подлинности. 
            Для получения кода проверки пользователя в данном приложении используется телефон и сообщения электронной почты
            Здесь можно указать собственный поставщик и подключить его. 
             */
            manager.RegisterTwoFactorProvider("Код, полученный по телефону", new PhoneNumberTokenProvider<ApplicationUser> {
                MessageFormat = "Ваш код безопасности: {0}"
            });
            manager.RegisterTwoFactorProvider("Код из сообщения", new EmailTokenProvider<ApplicationUser> {
                Subject = "Код безопасности",
                BodyFormat = "Ваш код безопасности: {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            IDataProtectionProvider dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

        public async Task<IdentityResult> DeleteUser(ApplicationUser user) {
            var email = user.Email;

            IdentityResult result = await this.DeleteAsync(user);

            if (result.Succeeded)
                await this.EmailService.SendAsync(new IdentityMessage() {
                    Destination = email,
                    Subject = "Удаление аккаунта",
                    Body = Helpers.MailMessageBodyCreator.CreateForDelete(user.UserName)
                });

            // delete
            // send email

            return result;
        }

        public async Task<IdentityResult> ResetPassword(ApplicationUser user) {
            var newPassword = System.Web.Security.Membership.GeneratePassword(10, 3) + "1";

            var token = await this.GeneratePasswordResetTokenAsync(user.Id);
            IdentityResult result = await this.ResetPasswordAsync(user.Id, token, newPassword);

            if (result.Succeeded)
            {
                user.HasTemporaryPassword = true;

                result = await this.UpdateAsync(user);

                if (result.Succeeded)
                    await this.EmailService.SendAsync(new IdentityMessage() {
                        Destination = user.Email,
                        Subject = "Смена пароля",
                        Body = Helpers.MailMessageBodyCreator.CreateForResetPassword(user.UserName, newPassword)
                    });
            }

            return result;
        }

        public async Task<IdentityResult> CreateUser(ApplicationUser user, string password, string Initials) {
            ApplicationUser declaredUser = await this.FindByEmailAsync(user.Email);

            if (declaredUser != null)
                return new IdentityResult("Учетная запись с данным почтовым адресом уже зарегистрирована.");

            //declaredUser = await this.FindByNameAsync(user.UserName);

            //if (declaredUser != null)
            //    user.UserName = user.Email.Substring(0, user.Email.IndexOf("@")); // изменить логику

            IdentityResult result = await this.CreateAsync(user, password);

            if (result.Succeeded)
            {
                var message = Helpers.MailMessageBodyCreator.CreateForRegist(
                    Initials,
                    user.UserName,
                    password,
                    "http://employee-cat.com");

                await this.SendEmailAsync(user.Id, "Регистрация в каталоге сотрудников", message);
            }

            // check if email registed
            // regist
            // send email

            return result;
        }
    }

    // Настройка диспетчера входа для приложения.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string> {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager) {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user) {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context) {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
