﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MAS.Work.EmployeeCatalog.Startup))]
namespace MAS.Work.EmployeeCatalog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
