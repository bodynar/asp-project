﻿namespace MAS.Work.EmployeeCatalog
{
    #region References
    using System.Data.Entity;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using Ef;
    using MAS.Work.EmployeeCatalog.Configurations;
    using MAS.Work.EmployeeCatalog.Mappers;
    #endregion

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            NLog.LogManager.GetCurrentClassLogger().Info("\nProject startup-ed");

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer(new AppDbInitializer());

            AutoMapperConfig.ConfigureMapper();

            MainConfiguration.Create().Configurate();
        }
    }
}
