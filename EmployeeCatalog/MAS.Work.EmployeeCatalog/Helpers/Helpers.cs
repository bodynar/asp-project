﻿namespace MAS.Work.EmployeeCatalog.Helpers {
    #region References

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using ViewModels;
    #endregion

    public static class MailMessageBodyCreator {
        static string messageBody;

        static MailMessageBodyCreator() {
            messageBody = @"<p>Доброго времени суток, @Name!</p>
[$body]
<hr />
<div>
<p>&nbsp;Дата $ACTION учетной записи: @DateTime.</p>
<p><em>С уважением,</em><br /><em>команда emp.cat.</em></p>
<p>@Year.</p>
</div>";
        }

        #region Body variants

        static string GetRegisterBody() {
            return
            messageBody.Replace("[$body]", @"<p>Для вас была создана учетная запись в каталоге сотрудников.<br />Каталог находится по адресу <a href=""http://localhost:2265/"" target=""_blank"">@Adress</a>.</p>
<p>Данные для входа в учетную запись:</p>
<div style=""padding-left: 30px;"">
<p>Логин: <strong>@Login</strong><br />Пароль: <strong>@Password</strong></p>
</div>
<p><em>Также для входа в учетную запись вы можете использовать в качестве логина данный почтовый адрес.</em></p>
<p><strong>После авторизации в системе, пожалуйста, смените пароль.</strong></p>");
        }

        static string GetDeleteBody() {
            return messageBody.Replace("[$body]", @"<p>Сообщаем вам, что ваша учетная запись в <a href=""http://localhost:2265/"">каталоге сотрудников</a>&nbsp;была удалена.</p>");
        }

        static string GetResetPasswordBody() {
            return
             messageBody.Replace("[$body]", @"<p>Для вашей учетной записи было запрошено восстановление пароля.</p>
<p>Новый временный пароль:</p>
<div>
<p style=""padding-left: 30px;""><strong>@Passwd</strong></p>
</div>
<p><strong>После авторизации в системе, пожалуйста, смените пароль.</strong></p>");
        }

        static string GetLoginChangeBody() {
            return
             messageBody.Replace("[$body]", @"<p>Логин вашей учетной записи был изменен администратором.</p>
<p>Новый логин:</p>
<div>
<p style=""padding-left: 30px;""><strong>@newLogin</strong></p>
</div>
<p><strong>Для авторизации используйте данный логин</strong>.</p>");
        }

        #endregion

        // вот это, конечно, супер [сарказм]

        #region Creation API (почти)

        public static string CreateForRegist(
            string Initials, string Login,
            string Password, string Adress = "")

            => GetRegisterBody().Replace("@Name", Initials)
                                .Replace("@Login", Login)
                                .Replace("@Password", Password)
                                .Replace("@Adress", Adress)
                                .Replace("@DateTime", DateTime.Now.ToShortDateString())
                                .Replace("@Year", DateTime.Now.Year.ToString())
                                .Replace("$ACTION", "создания");

        public static string CreateForDelete(
            string UserName)
            => GetDeleteBody().Replace("@Name", UserName)
                              .Replace("@DateTime", DateTime.Now.ToShortDateString())
                              .Replace("@Year", DateTime.Now.Year.ToString())
                              .Replace("$ACTION", "удаления");

        public static string CreateForResetPassword(string userName,
            string passwd)
            => GetResetPasswordBody().Replace("@Name", userName)
                                     .Replace("@Passwd", passwd)
                                     .Replace("@DateTime", DateTime.Now.ToShortDateString())
                                     .Replace("@Year", DateTime.Now.Year.ToString())
                                     .Replace("$ACTION", "смены пароля");

        public static string CreateForLoginChange(string ShortName, string userName)
            => GetLoginChangeBody().Replace("@Name", ShortName)
                                   .Replace("@newLogin", userName)
                                   .Replace("@DateTime", DateTime.Now.ToShortDateString())
                                   .Replace("@Year", DateTime.Now.Year.ToString())
                                   .Replace("$ACTION", "смены логина");

        #endregion
    }

    public static class ImageHelper {
        public static byte[] CropImage(byte[] content, int x, int y, int width, int height, double angle = 0) {
            using (MemoryStream stream = new MemoryStream(content))
            {
                return CropImage(stream, x, y, width, height, angle);
            }
        }

        public static byte[] CropImage(Stream content, int x, int y, int width, int height, double angle = 0) {
            using (var sourceBitmap = new Bitmap(content))
            {
                var flip = GetRotateEnum(angle);

                sourceBitmap.RotateFlip(flip);

                var sourceWidth = Convert.ToDouble(sourceBitmap.Size.Width);
                var sourceHeight = Convert.ToDouble(sourceBitmap.Size.Height);

                var cropRect = new Rectangle(x, y, width, height);

                using (var newBitMap = new Bitmap(cropRect.Width, cropRect.Height))
                {
                    using (var g = Graphics.FromImage(newBitMap))
                    {
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        g.CompositingQuality = CompositingQuality.HighQuality;

                        g.DrawImage(sourceBitmap,
                            new Rectangle(0, 0, newBitMap.Width, newBitMap.Height),
                            cropRect, GraphicsUnit.Pixel);

                        return GetBitmapBytes(newBitMap);
                    }
                }
            }
        }

        static byte[] GetBitmapBytes(Bitmap source) {
            var codec = ImageCodecInfo.GetImageEncoders()[4];
            var parameters = new EncoderParameters(1);
            parameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            using (var tmpStream = new MemoryStream())
            {
                source.Save(tmpStream, codec, parameters);

                byte[] result = new byte[tmpStream.Length];
                tmpStream.Seek(0, SeekOrigin.Begin);
                tmpStream.Read(result, 0, (int)tmpStream.Length);

                return result;
            }
        }

        static RotateFlipType GetRotateEnum(double angle) {
            angle = (angle + 360) % 360;

            switch (angle.ToString())
            {
                case "90":
                    return RotateFlipType.Rotate90FlipNone;

                case "180":
                    return RotateFlipType.Rotate180FlipNone;

                case "270":
                    return RotateFlipType.Rotate270FlipNone;

                default:
                    return RotateFlipType.RotateNoneFlipNone;
            }
        }

        public static string ConvertByteToBase64(byte[] photo)
            => string.Format("data:image/png;base64,{0}", Convert.ToBase64String(photo));

        public static byte[] ConvertBase64ToByte(string base64) {
            var ind = base64.IndexOf("base64,");
            return Convert.FromBase64String(base64.Substring(ind + 7));
        }

        public static string GetImageFromFileAsBase64(string path) {
            byte[] bytes = File.ReadAllBytes(path);
            return ConvertByteToBase64(bytes);
        }
    }

    public static class TranslatinationHelper {
        static Dictionary<char, string> ru2eng = new Dictionary<char, string>();
        // static Dictionary<string, string> eng2ru = new Dictionary<string, string>(); // ох уж эти названия

        static TranslatinationHelper() {
            #region ru -> eng

            ru2eng.Add('а', "a");
            ru2eng.Add('б', "b");
            ru2eng.Add('в', "v");
            ru2eng.Add('г', "g");
            ru2eng.Add('д', "d");
            ru2eng.Add('е', "e");
            ru2eng.Add('ё', "yo");
            ru2eng.Add('ж', "zh");
            ru2eng.Add('з', "z");
            ru2eng.Add('и', "i");
            ru2eng.Add('й', "j");
            ru2eng.Add('к', "k");
            ru2eng.Add('л', "l");
            ru2eng.Add('м', "m");
            ru2eng.Add('н', "n");
            ru2eng.Add('о', "o");
            ru2eng.Add('п', "p");
            ru2eng.Add('р', "r");
            ru2eng.Add('с', "s");
            ru2eng.Add('т', "t");
            ru2eng.Add('у', "u");
            ru2eng.Add('ф', "f");
            ru2eng.Add('х', "kh");
            ru2eng.Add('ц', "c");
            ru2eng.Add('ч', "ch");
            ru2eng.Add('ш', "sh");
            ru2eng.Add('щ', "sch");
            ru2eng.Add('ъ', "j");
            ru2eng.Add('ы', "i");
            ru2eng.Add('ь', "j");
            ru2eng.Add('э', "e");
            ru2eng.Add('ю', "yu");
            ru2eng.Add('я', "ya");

            #endregion

            #region eng -> ru

            //eng2ru.Add("q", "ку");
            // qwertyuiop asdfghjkl zxcvbnm

            #endregion
        }

        static string RuToEng(string ruString) {
            var result = "";
            ruString = ruString.ToLower();

            foreach (var ch in ruString)
                result += RuToEng(ch);

            return result;
        }

        static string RuToEng(char ruChar)
            => ru2eng[ruChar];


        static string EngToRu(string innerRow) {
            throw new NotImplementedException();
            /*
            var result = "";

            foreach (var ch in innerRow)
            {
                if (eng2ru.ContainsKey(ch.ToString().ToLower())) // обожэмой
                    result += eng2ru[ch.ToString().ToLower()];
            }

            return result;
            */
        }

        public static string CreateUserName(string secondName, string firstName, string middleName) {
            var isRu = StringIsRu(secondName) && StringIsRu(firstName);
            var result = "";

            if (isRu)
            {
                secondName = RuToEng(secondName);
                firstName = RuToEng(firstName);
                if (!string.IsNullOrEmpty(middleName))
                    middleName = RuToEng(middleName);
            }

            var md = string.IsNullOrEmpty(middleName) ? "" : "." + middleName[0];

            result = secondName + "." + firstName[0] + md;

            return result;
        }

        static bool StringIsRu(string row) {
            bool result = true;
            row = row.ToLower();

            foreach (var ch in row)
            {
                var code = (int)ch;
                if (!(code >= 1072 && code <= 1103)) // eng - 97-123
                    result = false;
            }

            return result;
        }
    }

    public static class HtmlExtensions {
        public static MvcHtmlString If(this MvcHtmlString value, bool evaluation) {
            return evaluation ? value : MvcHtmlString.Empty;
        }
        public static MvcHtmlString Concate(this MvcHtmlString value, Func<string> fun) {
            return MvcHtmlString.Create(value + fun());
        }

        public static MvcHtmlString PageLinks(this HtmlHelper html,
        PageInfo pageInfo, Func<int, string> pageUrl) {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();

                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}