﻿namespace MAS.Work.EmployeeCatalog.Helpers
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.IO;
    using NReco.VideoConverter;
    using NReco.VideoInfo;
    #endregion

    public class VideoThumbnail
    {
        public string PathToFile { get; set; }

        public float TimePoint { get; set; }

        public static VideoThumbnail Create()
            => new VideoThumbnail();

    }

    public class FFMpegWorker : IDisposable
    {
        protected FFMpegConverter _worker;

        protected string _tempFolder;
        public string TempFolder
            => _tempFolder;

        private FFMpegWorker() { }

        public FFMpegWorker(string tempFolder)
        {
            _tempFolder = CreateSubFolder(tempFolder);

            _worker = new FFMpegConverter();
        }

        public static FFMpegWorker Create(string tempFolder)
            => new FFMpegWorker(tempFolder);

        string CreateSubFolder(string originTempPath)
        {
            var path = Path.Combine(originTempPath, Guid.NewGuid().ToString());
            Directory.CreateDirectory(path);

            return path;
        }

        public MediaInfo GetVideoInfo(string pathToVideoFile)
            => new FFProbe().GetMediaInfo(pathToVideoFile);

        public IEnumerable<VideoThumbnail> GetThumbnails(string pathToVideo, int count)
        {
            var videoInfo = GetVideoInfo(pathToVideo);
            var duration = videoInfo.Duration;

            var result = new List<VideoThumbnail>(); // *

            if (duration.TotalMinutes < 5)
            {
                result.Add(GenerateThumbNail(pathToVideo,
                    Path.Combine(TempFolder, "single-preview.jpg"),
                   //new TimeSpan(0, 0, 5).ToString(@"hh\:mm\:ss"))
                   (float)new TimeSpan(0, 0, 0).TotalSeconds
                   ));

                return result;
            }

            var first = new VideoThumbnail()
            {
                PathToFile = Path.Combine(TempFolder, "first-preview.jpg"),
                TimePoint = (float)(new TimeSpan(0, 0, 0, 0).Add(new TimeSpan(0, 1, 30)).TotalSeconds)
                //.ToString(@"hh\:mm\:ss"),
            };

            result.Add(GenerateThumbNail(pathToVideo, first.PathToFile, first.TimePoint)); // *

            var last = new VideoThumbnail()
            {
                PathToFile = Path.Combine(TempFolder, "last-preview.jpg"),
                TimePoint = (float)duration.Subtract(new TimeSpan(0, 1, 30)).TotalSeconds
                //.ToString(@"hh\:mm\:ss"),
            };

            result.Add(GenerateThumbNail(pathToVideo, last.PathToFile, last.TimePoint)); // *

            var iteration = (int)duration.TotalMinutes / (count - 2);

            for (var i = iteration; i < (int)duration.TotalMinutes; i += iteration)
            {
                var timePoint = new TimeSpan(0, 0, i, 0, 0);

                var temp = new VideoThumbnail()
                {
                    PathToFile = Path.Combine(TempFolder, i + "-preview.jpg"),
                    TimePoint = (float)timePoint.TotalSeconds
                    //.ToString(@"hh\:mm\:ss"),
                };

                result.Add(GenerateThumbNail(pathToVideo, temp.PathToFile, temp.TimePoint)); // *
            }

            return result;
        }

        VideoThumbnail GenerateThumbNail(string pathToVideo, string pathToResult, float TimePoint)
        {
            var thumbInfo = new VideoThumbnail()
            {
                PathToFile = pathToResult,
                TimePoint = TimePoint,
            };

            var result = GenerateThumbnail(pathToVideo, thumbInfo);

            if (!result.Succeeded)
            {
                // handle exc
            }

            return result.Result;
        }

        IActionResultProvider<VideoThumbnail> GenerateThumbnail(string pathToVideo, VideoThumbnail thumbnailTemplate)
        {
            try
            {
                _worker.GetVideoThumbnail(pathToVideo, thumbnailTemplate.PathToFile, thumbnailTemplate.TimePoint);

                return ResultProvider<VideoThumbnail>.Succeded(thumbnailTemplate);
            }
            catch (Exception exc)
            {
                return ResultProvider<VideoThumbnail>.Failed(exc);
            }
        }

        public void Dispose()
        {
            if (Directory.Exists(TempFolder))
                Directory.Delete(TempFolder, true);
        }
    }

    public class VideoFile
    {
        #region Properties
        public string Path { get; set; }
        public TimeSpan Duration { get; set; }
        public double BitRate { get; set; }
        public string AudioFormat { get; set; }
        public string VideoFormat { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string RawInfo { get; set; }
        #endregion

        public VideoFile(string path)
        {
            Path = path;
        }

        public static VideoFile Create(string path)
            => new VideoFile(path);
    }
}