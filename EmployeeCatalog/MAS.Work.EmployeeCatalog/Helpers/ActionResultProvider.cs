﻿namespace MAS.Work.EmployeeCatalog.Helpers
{
    #region References
    using System;
    #endregion
    public interface IActionResultProvider<T>
    {
        Exception Exc { get; set; }

        T Result { get; set; }

        bool Succeeded { get; }
    }

    public class ResultProvider<T> : IActionResultProvider<T>
    {
        public Exception Exc { get; set; }

        public T Result { get; set; }

        public bool Succeeded
             => Exc == null && Result != null;

        public static ResultProvider<T> Succeded(T result)
            => new ResultProvider<T>() { Exc = null, Result = result };

        public static ResultProvider<T> Failed(Exception exc)
            => new ResultProvider<T>() { Exc = exc, Result = default(T) };
    }
}