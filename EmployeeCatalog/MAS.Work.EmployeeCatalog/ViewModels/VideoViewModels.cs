﻿namespace MAS.Work.EmployeeCatalog.ViewModels {
    #region References

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web;
    using Models;
    #endregion

    public class VideoPreviewViewModel {
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        public string Duration { get; set; }

        [Display(Name = "Рейтинг")]
        public double Rate { get; set; }

        [Display(Name = "Превью")]
        public string Preview { get; set; }

        [Display(Name = "Загружено")]
        public string Author { get; set; }

        [Display(Name = "Лектор")]
        public string Lector { get; set; }

        [Display(Name = "Категория")]
        public string Category { get; set; }

        [Display(Name = "Комментарии")]
        public int CommentsCount { get; set; }

        public string UploadDate { get; set; }

        [Display(Name = "Описание")]
        public string PartOfDecription { get; set; }
    }

    public class VideoEditViewModel {
        public int? Id { get; set; }
        public string FileName { get; set; }
        public string AuthorId { get; set; }

        public string Preview { get; set; }

        public TimeSpan Duration { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 3, ErrorMessage = "Название должно быть не длинее 40 символов")]
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }

        public int VideoCategoryId { get; set; }

        public string LectorId { get; set; }
    }

    public class VideoWatchViewModel {
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Рейтинг")]
        public double Rate { get; set; }

        [Display(Name = "Расположение на сервере")]
        public string FileName { get; set; }

        [Display(Name = "Лектор")]
        public string Lector { get; set; }

        [Display(Name = "Категория")]
        public string Category { get; set; }
    }
}