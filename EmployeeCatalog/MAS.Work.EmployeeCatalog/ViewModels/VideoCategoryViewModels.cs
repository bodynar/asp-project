﻿namespace MAS.Work.EmployeeCatalog.ViewModels
{
    #region References
    using System.ComponentModel.DataAnnotations;
    #endregion

    public class VideoCategoryEditViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }
    }

    public class VideoCategoryCreateViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }
    }

    public class VideoCategoryInfoViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Название")]

        public string Name { get; set; }
        [Display(Name ="Количество видео-роликов")]
        public int Count { get; set; }
    }
}