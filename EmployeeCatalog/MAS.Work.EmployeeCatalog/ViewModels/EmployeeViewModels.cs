﻿namespace MAS.Work.EmployeeCatalog.ViewModels
{
    #region References

    using System;
    using System.ComponentModel.DataAnnotations;

    #endregion

    public class EmployeeViewModel
    {
        public int? Id { get; set; }

        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public string SecondName { get; set; }

        [DataType(DataType.Date)]
        [Required]
        [Display(Name = "Дата рождения")]
        public DateTime BirthDay { get; set; }

        [Display(Name = "ФИО")]
        public string Initals 
        {
            get
            {
                var md = string.IsNullOrEmpty(MiddleName) ? "" : MiddleName[0] + "."; // fix zis

                return SecondName + " " + FirstName[0] + ". " + md;
            }
        }

        public int? PostId { get; set; }

        public int? DepartamentId { get; set; }

        public string Preview { get; set; }
    }


    public class PostViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Сотрудники")]
        public int Count { get; set; }
    }

    public class OrganizationViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название организации")]
        public string Name { get; set; }
    }

    public class DepartamentViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название отдела")]
        public string Name { get; set; }

        [Display(Name = "Цели отдела")]
        public string[] Aims { get; set; }

        public int OrganizationId { get; set; }

        [Display(Name = "Организация")]
        public string OrgName { get; set; }
    }
}