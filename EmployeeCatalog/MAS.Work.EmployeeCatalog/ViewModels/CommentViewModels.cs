﻿namespace MAS.Work.EmployeeCatalog.ViewModels
{
    #region References
    using System;
    using System.ComponentModel.DataAnnotations;
    #endregion

    public class CommentAddViewModel
    {
        [Required]
        [Display(Name ="Текст")]
        public string Text { get; set; }

        public int LectureId { get; set; }
    }

    public class CommentPreviewViewModel
    {
        public string Date { get; set; }
        public int Id { get; set; }
        public string Text { get; set; }
        public string AuthorAvatar { get; set; }
        public string Author { get; set; }
        public string AuthorId { get; set; }
    }
}