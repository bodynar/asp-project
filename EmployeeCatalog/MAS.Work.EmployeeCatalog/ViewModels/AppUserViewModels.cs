﻿namespace MAS.Work.EmployeeCatalog.ViewModels
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Microsoft.AspNet.Identity.EntityFramework;
    #endregion

    public class AppUserLoginViewModel
    {
        [Display(Name = "E-mail")]
        [EmailAddress(ErrorMessage = "Повторите ввод e-mail")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина почтового адреса должна быть не менее {2} символов")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле {0} не заполнено")]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Длина пароля должна быть не менее {2} символов")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }

    public class AppUserRegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Имя")]
        [Required]
        [StringLength(50, ErrorMessage = "Значение поля {0} должно содержать не менее {2} символов", MinimumLength = 1)]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        [StringLength(50, ErrorMessage = "Значение поля {0} должно содержать не менее {2} символов", MinimumLength = 1)]
        public string SecondName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime? BirthDay { get; set; }

        [Display(Name = "Отдел")]
        [Required]
        public int? DepartamentId { get; set; }

        [Display(Name = "Должность")]
        [Required]
        public int? PostId { get; set; }
        public string Preview { get; set; }
    }

    public class AppUserInfoViewModel
    {
        [Required]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public string SecondName { get; set; }

        [Display(Name = "Возраст")]
        public int Age
        {
            get
            {
                var age = DateTime.Now.Year - birthday_.Year;

                if (DateTime.Now.Month < birthday_.Month ||
                    (DateTime.Now.Month == birthday_.Month && DateTime.Now.Day < birthday_.Day))
                    age--;

                return age;
            }
        }

        public DateTime birthday_ { get; set; }

        [Display(Name = "Дата рождения")]
        public string BirthDay
        {
            get { return birthday_.ToLongDateString(); }
        }

        [Display(Name = "ФИО")]
        public string Initals { get { return SecondName + " " + FirstName[0] + ". " + MiddleName[0] + "."; } }

        [Display(Name = "Е-мейл")]
        public string Email { get; set; }

        [Display(Name = "Никнейм")]
        public string UserName { get; set; }

        public DateTime Register { get; set; }

        [Display(Name = "Дата регистрации")]
        public string RegisterDate
        {
            get { return Register.ToLongDateString(); }
        }
    }

    public class AppUserEditViewModel
    {
        [Display(Name = "Логин")]
        [Required]
        [StringLength(50, ErrorMessage = "Значение поля {0} должно содержать не менее {2} символов", MinimumLength = 1)]
        public string UserName { get; set; }

        [Display(Name = "Имя")]
        [Required]
        [StringLength(50, ErrorMessage = "Значение поля {0} должно содержать не менее {2} символов", MinimumLength = 1)]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        [StringLength(50, ErrorMessage = "Значение поля {0} должно содержать не менее {2} символов", MinimumLength = 1)]
        public string SecondName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime? BirthDay { get; set; }

        [Required]
        public int DepartamentId { get; set; }

        [Required]
        public int PostId { get; set; }

        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public string UserId { get; set; }

        public string Preview { get; set; }
    }

    public class AppUserChangePasswordViewModel
    {
        public string UserId { get; set; }

        [Display(Name = "Старый пароль")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать символов не менее: {2}.", MinimumLength = 6)]
        [Required]
        public string OldPassword { get; set; }

        [Display(Name = "Новый пароль")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать символов не менее: {2}.", MinimumLength = 6)]
        [Required]
        public string NewPassword { get; set; }

        [Display(Name = "Подтверждение пароля")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать символов не менее: {2}.", MinimumLength = 6)]
        [Required]
        public string NewPasswordConfirmation { get; set; }
    }

    public class AppUserManageViewModel
    {
        public bool HasTemporaryPassword { get; set; }

        public string PhoneNumber { get; set; }

        public bool TwoFactorEnabled { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public int PhoneNumbers { get; set; }
    }

    public class RoleInfoViewModel
    {
        public string Id { get; set; }

        [Display(Name = "Название роли")]
        public string Name { get; set; }

        [Display(Name = "Количество пользователей")]
        public int Count { get; set; }
    }

    public class RoleCreateViewModel
    {
        [StringLength(50, ErrorMessage = "Длина названия должна быть не более {0} символов", MinimumLength = 3)]
        [Display(Name = "Название")]
        public string Name { get; set; }

        public IEnumerable<IdentityUser> Users { get; set; }
    }

    public class RoleEditViewModel
    {
        public string Id { get; set; }

        [StringLength(50, ErrorMessage = "Длина названия должна быть не более {0} символов", MinimumLength = 3)]
        [Required]
        [Display(Name="Название")]
        public string Name { get; set; }
    }
}