﻿namespace MAS.Work.EmployeeCatalog.ViewModels
{
    #region References

    using System;
    using System.Collections.Generic;

    #endregion

    public class PageInfo
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages {
        get {

                int result = TotalItems / PageSize,
                    subResult = TotalItems % PageSize;

                if (subResult > 0)
                    result++;

                return result;
            }
        }

        public bool IsFirstPage
            => PageNumber == 1;

        public bool IsLastPage
            => PageNumber == TotalPages; 
    }

    public class PageViewModel<T>
    {
        public IEnumerable<T> Objects { get; set; }
        public PageInfo PageInfo { get; set; }

        public int Param { get; set; }
    }
}