namespace MAS.Work.EmployeeCatalog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedcommentmodel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Comments", new[] { "UserId" });
            RenameColumn(table: "dbo.Comments", name: "UserId", newName: "AuthorId");
            AlterColumn("dbo.Comments", "AuthorId", c => c.Int(nullable: false));
            CreateIndex("dbo.Comments", "AuthorId");
            AddForeignKey("dbo.Comments", "AuthorId", "dbo.Employees", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "AuthorId", "dbo.Employees");
            DropIndex("dbo.Comments", new[] { "AuthorId" });
            AlterColumn("dbo.Comments", "AuthorId", c => c.String(maxLength: 128));
            RenameColumn(table: "dbo.Comments", name: "AuthorId", newName: "UserId");
            CreateIndex("dbo.Comments", "UserId");
            AddForeignKey("dbo.Comments", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
