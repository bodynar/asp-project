namespace MAS.Work.EmployeeCatalog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Updatedvideomodelbyaddingboolvalue : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Videos", "IsReady", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Videos", "IsReady");
        }
    }
}
