﻿namespace MAS.Work.EmployeeCatalog.Repositories
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Ef;

    #endregion

    interface IRepository<T>
        where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        IEnumerable<T> GetByPredicate(Expression<Func<T, bool>> predicate);
        T GetByPredicateSingle(Expression<Func<T, bool>> predicate);
        void Add(T item);
        void Update(T item);
        void Delete(int id);
        void Delete(T item);
        void DeleteMultiple(IEnumerable<T> items);
    }
    
    public abstract class AppDbRepositoryBase<T> : IRepository<T>
       where T : class
    {
        private AppDbContext db;

        public abstract IEnumerable<T> GetAll();
        public abstract T GetById(int id);
        public abstract T GetByPredicateSingle(Expression<Func<T, bool>> predicate);
        public abstract IEnumerable<T> GetByPredicate(Expression<Func<T, bool>> predicate);
        public abstract void Add(T obj);
        public abstract void Update(T obj); // Is it correcto?
        public abstract void Delete(int id);
        public abstract void Delete(T item);
        public abstract void DeleteMultiple(IEnumerable<T> items);
    }
}