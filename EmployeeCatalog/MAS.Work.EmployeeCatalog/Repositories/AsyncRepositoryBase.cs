﻿namespace MAS.Work.EmployeeCatalog.Repositories
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using MAS.Work.EmployeeCatalog.Ef;
    #endregion
    interface IAsyncRepository<T>
         where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task<IEnumerable<T>> GetByPredicate(Expression<Func<T, bool>> predicate);
        Task<T> GetByPredicateSingle(Expression<Func<T, bool>> predicate);
        Task Add(T item);
        Task Update(T item);
        Task Delete(int id);
        void Delete(T item);
        void DeleteMultiple(IEnumerable<T> items);
    }
    public abstract class AppDbAsyncRepositoryBase<T> : IAsyncRepository<T>
        where T : class
    {
        private AppDbContext db;

        public abstract Task<IEnumerable<T>> GetAll();
        public abstract Task<T> GetById(int id);
        public abstract Task<T> GetByPredicateSingle(Expression<Func<T, bool>> predicate);
        public abstract Task<IEnumerable<T>> GetByPredicate(Expression<Func<T, bool>> predicate);
        public abstract Task Add(T obj);
        public abstract Task Update(T obj); // Is it correcto?
        public abstract Task Delete(int id);
        public abstract void Delete(T item);
        public abstract void DeleteMultiple(IEnumerable<T> items);
    }
}