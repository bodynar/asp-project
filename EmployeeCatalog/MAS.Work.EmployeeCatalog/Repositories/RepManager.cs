﻿namespace MAS.Work.EmployeeCatalog.Repositories
{
    #region References

    using System;
    using System.Threading.Tasks;
    using Ef;

    #endregion

    public class RepManager : IDisposable
    {
        private EmployeeRepository _emps;
        private DepartamentRepository _deps;
        private OrganizationRepository _orgs;
        private ManInfoRepository _maninfos;
        private PostRepository _posts;
        private CommentRepository _comments;
        private VideoCategoryRepository _videocats;
        private VideoRepository _videos;

        private AppDbContext db;

        private RepManager(AppDbContext db)
        {
            if (db == null)
                throw new NullReferenceException(message: "Db Context can't be null");

            this.db = db;
        }

        public static RepManager Create(AppDbContext db)
            => new RepManager(db);

        #region Reps

        public EmployeeRepository Employees
            => _emps ?? (_emps = EmployeeRepository.Create(db));

        public DepartamentRepository Departaments
            => _deps ?? (_deps = DepartamentRepository.Create(db));

        public OrganizationRepository Organizations
            => _orgs ?? (_orgs = OrganizationRepository.Create(db));

        public PostRepository Posts
            => _posts ?? (_posts = PostRepository.Create(db));

        public ManInfoRepository ManInfos
            => _maninfos ?? (_maninfos = ManInfoRepository.Create(db));

        public CommentRepository Comments
            => _comments ?? (_comments = CommentRepository.Create(db));


        public VideoRepository Videos
            => _videos ?? (_videos = VideoRepository.Create(db));

        public VideoCategoryRepository VideoCategories
            => _videocats ?? (_videocats = VideoCategoryRepository.Create(db));

        #endregion

        public async Task SaveChangesAsync()
            => await db.SaveChangesAsync();

       /* test
        * public async Task<ResultProvider<bool>> SaveChangesAsync()
        {
            try
            {
                await db.SaveChangesAsync();
                return ResultProvider<bool>.Succeded(true);
            }
            catch (Exception e)
            {
                return ResultProvider<bool>.Failed(e);
            }
        }
        */

        public void SaveChanges()
            => db.SaveChanges();

        public void Dispose()
        {
            db?.Dispose();
            GC.Collect();
        }
    }
}