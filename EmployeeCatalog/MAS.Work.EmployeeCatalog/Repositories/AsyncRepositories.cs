﻿namespace MAS.Work.EmployeeCatalog.Repositories
{
    #region References
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using MAS.Work.EmployeeCatalog.Ef;
    using MAS.Work.EmployeeCatalog.Models;
    #endregion
    public sealed class EmployeeAsyncRepository : AppDbAsyncRepositoryBase<Employee>
    {
        private AppDbContext db;

        private EmployeeAsyncRepository(AppDbContext db) { this.db = db; }

        public static EmployeeAsyncRepository Create(AppDbContext db)
            => new EmployeeAsyncRepository(db);

        public override async Task<IEnumerable<Employee>> GetAll()
            => await db.Employees.ToListAsync();

        public override async Task<Employee> GetById(int id)
            => await db.Employees.FirstOrDefaultAsync(x => x.Id == id);

        public override async Task<Employee> GetByPredicateSingle(Expression<Func<Employee, bool>> predicate)
            => await db.Employees.SingleOrDefaultAsync(predicate);

        public override async Task<IEnumerable<Employee>> GetByPredicate(Expression<Func<Employee, bool>> predicate)
            => await db.Employees.Where(predicate).ToListAsync();

        public override async Task Add(Employee entity)
            => await Task.Run(() => db.Employees.Add(entity));

        public override async Task Update(Employee entity)
            => await Task.Run(() => db.Entry(entity).State = EntityState.Modified);

        public override async Task Delete(int id)
            => await Task.Run(async () =>
            {
                var entity = await GetById(id);
                if (entity != null)
                    db.Employees.Remove(entity);
            });

        public override void Delete(Employee item)
            => db.Employees.Remove(item);

        public override void DeleteMultiple(IEnumerable<Employee> items)
            => db.Employees.RemoveRange(items);

    }
    public sealed class DepartamentAsyncRepository : AppDbAsyncRepositoryBase<Departament>
    {
        private AppDbContext db;

        private DepartamentAsyncRepository(AppDbContext db) { this.db = db; }

        public static DepartamentAsyncRepository Create(AppDbContext db)
            => new DepartamentAsyncRepository(db);

        public override async Task<IEnumerable<Departament>> GetAll()
            => await db.Departaments.ToListAsync();

        public override async Task<Departament> GetById(int id)
            => await db.Departaments.FirstOrDefaultAsync(x => x.Id == id);

        public override async Task<Departament> GetByPredicateSingle(Expression<Func<Departament, bool>> predicate)
            => await db.Departaments.SingleOrDefaultAsync(predicate);

        public override async Task<IEnumerable<Departament>> GetByPredicate(Expression<Func<Departament, bool>> predicate)
           => await db.Departaments.Where(predicate).ToListAsync();

        public override async Task Add(Departament entity)
            => await Task.Run(() => db.Departaments.Add(entity));

        public override async Task Update(Departament entity)
            => await Task.Run(() => db.Entry(entity).State = EntityState.Modified);

        public override async Task Delete(int id)
            => await Task.Run(async () =>
            {
                var entity = await GetById(id);
                if (entity != null)
                    db.Departaments.Remove(entity);
            });

        public override void Delete(Departament item)
            => db.Departaments.Remove(item);

        public override void DeleteMultiple(IEnumerable<Departament> items)
            => db.Departaments.RemoveRange(items);
    }
    public sealed class OrganizationAsyncRepository : AppDbAsyncRepositoryBase<Organization>
    {
        private AppDbContext db;

        private OrganizationAsyncRepository(AppDbContext db) { this.db = db; }

        public static OrganizationAsyncRepository Create(AppDbContext db)
            => new OrganizationAsyncRepository(db);

        public override async Task<IEnumerable<Organization>> GetAll()
            => await db.Organizations.ToListAsync();

        public override async Task<Organization> GetById(int id)
            => await db.Organizations.FirstOrDefaultAsync(x => x.Id == id);

        public override async Task<Organization> GetByPredicateSingle(Expression<Func<Organization, bool>> predicate)
            => await db.Organizations.SingleOrDefaultAsync(predicate);

        public override async Task<IEnumerable<Organization>> GetByPredicate(Expression<Func<Organization, bool>> predicate)
           => await db.Organizations.Where(predicate).ToListAsync();

        public override async Task Add(Organization entity)
            => await Task.Run(() => db.Organizations.Add(entity)); // T0DO: TEST
        //=> db.Organizations.Add(entity);

        public override async Task Update(Organization entity)
            => await Task.Run(() => db.Entry(entity).State = EntityState.Modified);

        public override async Task Delete(int id)
            => await Task.Run(async () =>
            {
                var entity = await GetById(id);
                if (entity != null)
                    db.Organizations.Remove(entity);
            });

        public override void Delete(Organization item)
            => db.Organizations.Remove(item);

        public override void DeleteMultiple(IEnumerable<Organization> items)
            => db.Organizations.RemoveRange(items);
    }
    public sealed class ManInfoAsyncRepository : AppDbAsyncRepositoryBase<ManInfo>
    {
        private AppDbContext db;

        private ManInfoAsyncRepository(AppDbContext db) { this.db = db; }

        public static ManInfoAsyncRepository Create(AppDbContext db)
            => new ManInfoAsyncRepository(db);

        public override async Task<IEnumerable<ManInfo>> GetAll()
            => await db.ManInfos.ToListAsync();

        public override async Task<ManInfo> GetById(int id)
            => await db.ManInfos.FirstOrDefaultAsync(x => x.Id == id);

        public override async Task<ManInfo> GetByPredicateSingle(Expression<Func<ManInfo, bool>> predicate)
            => await db.ManInfos.SingleOrDefaultAsync(predicate);

        public override async Task<IEnumerable<ManInfo>> GetByPredicate(Expression<Func<ManInfo, bool>> predicate)
           => await db.ManInfos.Where(predicate).ToListAsync();

        public override async Task Add(ManInfo entity)
            => await Task.Run(() => db.ManInfos.Add(entity));

        public override async Task Update(ManInfo entity)
            => await Task.Run(() => db.Entry(entity).State = EntityState.Modified);

        public override async Task Delete(int id)
            => await Task.Run(async () =>
            {
                var entity = await GetById(id);
                if (entity != null)
                    db.ManInfos.Remove(entity);
            });

        public override void Delete(ManInfo item)
            => db.ManInfos.Remove(item);

        public override void DeleteMultiple(IEnumerable<ManInfo> items)
            => db.ManInfos.RemoveRange(items);
    }
    public sealed class PostAsyncRepository : AppDbAsyncRepositoryBase<Post>
    {
        private AppDbContext db;

        private PostAsyncRepository(AppDbContext db) { this.db = db; }

        public static PostAsyncRepository Create(AppDbContext db)
            => new PostAsyncRepository(db);

        public override async Task<IEnumerable<Post>> GetAll()
            => await db.Posts.ToListAsync();

        public override async Task<Post> GetById(int id)
            => await db.Posts.FirstOrDefaultAsync(x => x.Id == id);

        public override async Task<Post> GetByPredicateSingle(Expression<Func<Post, bool>> predicate)
            => await db.Posts.SingleOrDefaultAsync(predicate);

        public override async Task<IEnumerable<Post>> GetByPredicate(Expression<Func<Post, bool>> predicate)
           => await db.Posts.Where(predicate).ToListAsync();

        public override async Task Add(Post entity)
            => await Task.Run(() => db.Posts.Add(entity));

        public override async Task Update(Post entity)
            => await Task.Run(() => db.Entry(entity).State = EntityState.Modified);

        public override async Task Delete(int id)
            => await Task.Run(async () =>
            {
                var entity = await GetById(id);
                if (entity != null)
                    db.Posts.Remove(entity);
            });

        public override void Delete(Post item)
            => db.Posts.Remove(item);

        public override void DeleteMultiple(IEnumerable<Post> items)
            => db.Posts.RemoveRange(items);
    }
}
