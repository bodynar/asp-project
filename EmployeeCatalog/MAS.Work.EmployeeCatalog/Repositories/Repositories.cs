﻿namespace MAS.Work.EmployeeCatalog.Repositories {
    #region References

    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using Ef;
    using MAS.Work.EmployeeCatalog.Models;

    #endregion

    public sealed class EmployeeRepository : AppDbRepositoryBase<Employee> {
        private AppDbContext db;

        private EmployeeRepository(AppDbContext db) { this.db = db; }

        public static EmployeeRepository Create(AppDbContext db)
            => new EmployeeRepository(db);

        public override IEnumerable<Employee> GetAll()
            => db.Employees.ToList();

        public override Employee GetById(int id)
            => db.Employees.FirstOrDefault(x => x.Id == id);

        public override Employee GetByPredicateSingle(Expression<Func<Employee, bool>> predicate)
            => db.Employees.SingleOrDefault(predicate);

        public override IEnumerable<Employee> GetByPredicate(Expression<Func<Employee, bool>> predicate)
            => db.Employees.Where(predicate).ToList();

        public override void Add(Employee entity)
            => db.Employees.Add(entity);

        public override void Update(Employee entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.Employees.Remove(entity);
        }

        public override void Delete(Employee item)
            => db.Employees.Remove(item);

        public override void DeleteMultiple(IEnumerable<Employee> items)
            => db.Employees.RemoveRange(items);

        public string GetShortNameByUserId(string userId) {
            ApplicationUser user = db.Users.FirstOrDefault(usr => usr.Id == userId);
            Employee emp = db.Employees.FirstOrDefault(e => e.UserId == userId);

            return emp.ManInfo.Initals;
        }

        public string GetShortName(string userName) {
            ApplicationUser user = db.Users.FirstOrDefault(usr => usr.UserName == userName);
            var shortName = GetShortNameByUserId(user.Id);
            return shortName;
        }

        public Employee GetByUserId(string userId) {
            Employee emp = db.Employees.FirstOrDefault(e => e.UserId == userId);
            return emp;
        }
    }
    public sealed class DepartamentRepository : AppDbRepositoryBase<Departament> {
        private AppDbContext db;

        private DepartamentRepository(AppDbContext db) { this.db = db; }

        public static DepartamentRepository Create(AppDbContext db)
            => new DepartamentRepository(db);

        public override IEnumerable<Departament> GetAll()
            => db.Departaments.ToList();

        public override Departament GetById(int id)
            => db.Departaments.FirstOrDefault(x => x.Id == id);

        public override Departament GetByPredicateSingle(Expression<Func<Departament, bool>> predicate)
            => db.Departaments.SingleOrDefault(predicate);

        public override IEnumerable<Departament> GetByPredicate(Expression<Func<Departament, bool>> predicate)
            => db.Departaments.Where(predicate).ToList();

        public override void Add(Departament entity)
            => db.Departaments.Add(entity);

        public override void Update(Departament entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.Departaments.Remove(entity);
        }

        public override void Delete(Departament item)
            => db.Departaments.Remove(item);

        public override void DeleteMultiple(IEnumerable<Departament> items)
            => db.Departaments.RemoveRange(items);
    }
    public sealed class OrganizationRepository : AppDbRepositoryBase<Organization> {
        private AppDbContext db;

        private OrganizationRepository(AppDbContext db) { this.db = db; }

        public static OrganizationRepository Create(AppDbContext db)
            => new OrganizationRepository(db);

        public override IEnumerable<Organization> GetAll()
            => db.Organizations.ToList();

        public override Organization GetById(int id)
            => db.Organizations.FirstOrDefault(x => x.Id == id);

        public override Organization GetByPredicateSingle(Expression<Func<Organization, bool>> predicate)
            => db.Organizations.SingleOrDefault(predicate);

        public override IEnumerable<Organization> GetByPredicate(Expression<Func<Organization, bool>> predicate)
            => db.Organizations.Where(predicate).ToList();

        public override void Add(Organization entity)
            => db.Organizations.Add(entity);

        public override void Update(Organization entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.Organizations.Remove(entity);
        }

        public override void Delete(Organization item)
            => db.Organizations.Remove(item);

        public override void DeleteMultiple(IEnumerable<Organization> items)
            => db.Organizations.RemoveRange(items);
    }
    public sealed class ManInfoRepository : AppDbRepositoryBase<ManInfo> {
        private AppDbContext db;

        private ManInfoRepository(AppDbContext db) { this.db = db; }

        public static ManInfoRepository Create(AppDbContext db)
            => new ManInfoRepository(db);

        public override IEnumerable<ManInfo> GetAll()
            => db.ManInfos.ToList();

        public override ManInfo GetById(int id)
            => db.ManInfos.FirstOrDefault(x => x.Id == id);

        public override ManInfo GetByPredicateSingle(Expression<Func<ManInfo, bool>> predicate)
            => db.ManInfos.SingleOrDefault(predicate);

        public override IEnumerable<ManInfo> GetByPredicate(Expression<Func<ManInfo, bool>> predicate)
            => db.ManInfos.Where(predicate).ToList();

        public override void Add(ManInfo entity)
            => db.ManInfos.Add(entity);

        public override void Update(ManInfo entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.ManInfos.Remove(entity);
        }

        public override void Delete(ManInfo item)
            => db.ManInfos.Remove(item);

        public override void DeleteMultiple(IEnumerable<ManInfo> items)
            => db.ManInfos.RemoveRange(items);
    }
    public sealed class PostRepository : AppDbRepositoryBase<Post> {
        private AppDbContext db;

        private PostRepository(AppDbContext db) { this.db = db; }

        public static PostRepository Create(AppDbContext db)
            => new PostRepository(db);

        public override IEnumerable<Post> GetAll()
            => db.Posts.ToList();

        public override Post GetById(int id)
            => db.Posts.FirstOrDefault(x => x.Id == id);

        public override Post GetByPredicateSingle(Expression<Func<Post, bool>> predicate)
            => db.Posts.SingleOrDefault(predicate);

        public override IEnumerable<Post> GetByPredicate(Expression<Func<Post, bool>> predicate)
            => db.Posts.Where(predicate).ToList();

        public override void Add(Post entity)
            => db.Posts.Add(entity);

        public override void Update(Post entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.Posts.Remove(entity);
        }

        public override void Delete(Post item)
            => db.Posts.Remove(item);

        public override void DeleteMultiple(IEnumerable<Post> items)
            => db.Posts.RemoveRange(items);
    }
    public sealed class VideoRepository : AppDbRepositoryBase<Video> {
        private AppDbContext db;

        private VideoRepository(AppDbContext db) { this.db = db; }

        public static VideoRepository Create(AppDbContext db)
            => new VideoRepository(db);

        public override IEnumerable<Video> GetAll()
            => db.Videos.ToList();

        public override Video GetById(int id)
            => db.Videos.FirstOrDefault(x => x.Id == id);

        public override Video GetByPredicateSingle(Expression<Func<Video, bool>> predicate)
            => db.Videos.SingleOrDefault(predicate);

        public override IEnumerable<Video> GetByPredicate(Expression<Func<Video, bool>> predicate)
            => db.Videos.Where(predicate).ToList();

        public override void Add(Video entity)
            => db.Videos.Add(entity);

        public override void Update(Video entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.Videos.Remove(entity);
        }

        public override void Delete(Video item)
            => db.Videos.Remove(item);

        public override void DeleteMultiple(IEnumerable<Video> items)
            => db.Videos.RemoveRange(items);
    }
    public sealed class VideoCategoryRepository : AppDbRepositoryBase<VideoCategory> {
        private AppDbContext db;

        private VideoCategoryRepository(AppDbContext db) { this.db = db; }

        public static VideoCategoryRepository Create(AppDbContext db)
            => new VideoCategoryRepository(db);

        public override IEnumerable<VideoCategory> GetAll()
            => db.VideoCategorys.ToList();

        public override VideoCategory GetById(int id)
            => db.VideoCategorys.FirstOrDefault(x => x.Id == id);

        public override VideoCategory GetByPredicateSingle(Expression<Func<VideoCategory, bool>> predicate)
            => db.VideoCategorys.SingleOrDefault(predicate);

        public override IEnumerable<VideoCategory> GetByPredicate(Expression<Func<VideoCategory, bool>> predicate)
            => db.VideoCategorys.Where(predicate).ToList();

        public override void Add(VideoCategory entity)
            => db.VideoCategorys.Add(entity);

        public override void Update(VideoCategory entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.VideoCategorys.Remove(entity);
        }

        public override void Delete(VideoCategory item)
            => db.VideoCategorys.Remove(item);

        public override void DeleteMultiple(IEnumerable<VideoCategory> items)
            => db.VideoCategorys.RemoveRange(items);
    }
    public sealed class CommentRepository : AppDbRepositoryBase<Comment> {
        private AppDbContext db;

        private CommentRepository(AppDbContext db) { this.db = db; }

        public static CommentRepository Create(AppDbContext db)
            => new CommentRepository(db);

        public override IEnumerable<Comment> GetAll()
            => db.Comments.ToList();

        public override Comment GetById(int id)
            => db.Comments.FirstOrDefault(x => x.Id == id);

        public override Comment GetByPredicateSingle(Expression<Func<Comment, bool>> predicate)
            => db.Comments.SingleOrDefault(predicate);

        public override IEnumerable<Comment> GetByPredicate(Expression<Func<Comment, bool>> predicate)
            => db.Comments.Where(predicate).ToList();

        public override void Add(Comment entity)
            => db.Comments.Add(entity);

        public override void Update(Comment entity)
            => db.Entry(entity).State = EntityState.Modified;

        public override void Delete(int id) {
            var entity = GetById(id);
            if (entity != null)
                db.Comments.Remove(entity);
        }

        public override void Delete(Comment item)
            => db.Comments.Remove(item);

        public override void DeleteMultiple(IEnumerable<Comment> items)
            => db.Comments.RemoveRange(items);
    }
}