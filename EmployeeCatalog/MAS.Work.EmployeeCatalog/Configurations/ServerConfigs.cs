﻿namespace MAS.Work.EmployeeCatalog.Configurations
{
    #region References
    using System;
    using Helpers;
    #endregion

    public class MainConfiguration : IServerConfiguration
    {

        static MainConfiguration()
        {
            // add here
        }
        public IActionResultProvider<bool> Configurate()
        {
            try
            {
                // add here
                return ResultProvider<bool>.Succeded(true);
            }
            catch (Exception e)
            {
                return ResultProvider<bool>.Failed(e); 
            }
        }

        public IActionResultProvider<bool> InitializeFolders()
        {
            try
            {
                // TODO: Add here creating Videos folder & Videos/Temp
                return ResultProvider<bool>.Succeded(true);
            }
            catch (Exception e)
            {
                return ResultProvider<bool>.Failed(e);
            }
        }

        public static MainConfiguration Create()
            => new MainConfiguration();
    }
}