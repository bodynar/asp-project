﻿namespace MAS.Work.EmployeeCatalog.Configurations
{
    #region References
    using MAS.Work.EmployeeCatalog.Helpers;

    #endregion
    interface IServerConfiguration
    {
        IActionResultProvider<bool> Configurate();
        IActionResultProvider<bool> InitializeFolders();
    }
}
