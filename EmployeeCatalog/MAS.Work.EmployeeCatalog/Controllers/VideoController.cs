﻿namespace MAS.Work.EmployeeCatalog.Controllers {
    #region References

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using AutoMapper;
    using Helpers;
    using Microsoft.AspNet.Identity;
    using Models;
    using Validators;
    using ViewModels;
    using FileIO = System.IO.File;
    #endregion

    [Authorize]
    public class VideoController : BaseController {
        /* when implements DI
        private IVideoService _vidService;

        public IVideoService VideoService
        {
            get { return _vidService; }
            set { _vidService = value; }
        }


        public VideoController(IVideoService service)
        {
            VideoService = service;
        }
        */

        [HttpGet]
        public ActionResult Index()
            => View();

        [HttpGet]
        public ActionResult Videos(int? catId, string page) {
            int pageNum = string.IsNullOrEmpty(page) ? 1 : Convert.ToInt32(page);
            int pageSize_ = 15;

            IEnumerable<Video> src;
            // TODO: Организовать получение тех видосов, файлы которых есть
            if (catId.HasValue)
                src = RepManager.Videos.GetByPredicate(vid => vid.CategoryId == catId.Value);
            else
                src = RepManager.Videos.GetByPredicate(vid => vid.IsReady);

            var videos = src
                .OrderByDescending(x => x.UploadingDate)
                .Skip((pageNum - 1) * pageSize_)
                .Take(pageSize_)
                .ToList();

            var pageInfo = new PageInfo {
                PageNumber = pageNum,
                PageSize = pageSize_,
                TotalItems = src.Count()
            };

            var ivm = new PageViewModel<VideoPreviewViewModel> {
                PageInfo = pageInfo,
                Objects = videos.Select((x) => {
                    VideoPreviewViewModel res = Mapper.Map<Video, VideoPreviewViewModel>(x);
                    if (res.Lector != "Не назначен")
                        res.Lector = RepManager.Employees.GetShortName(userName: res.Lector);

                    if (string.IsNullOrEmpty(res.Preview))
                    {
                        var path = WebConfigurationManager.AppSettings["Images:Static:RelativePath"];
                        path = Path.Combine(Server.MapPath(path), "no-preview.png");
                        res.Preview = ImageHelper.GetImageFromFileAsBase64(path);
                    }

                    return res;
                }
                ),
                Param = catId ?? 0,
            };

            return PartialView("_Videos", ivm);
        }

        [HttpGet]
        public ActionResult ViewVideo(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Video vid = RepManager.Videos.GetById(id.Value);

            if (vid == null || !FileIO.Exists(vid.Path))
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            /*
            if (!vid.IsConverted || !vid.IsReady)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden); // or handle somehow
            */

            VideoWatchViewModel vm = Mapper.Map<Video, VideoWatchViewModel>(vid);

            if (vm.Lector != "Не назначен")
                vm.Lector = RepManager.Employees.GetShortName(vm.Lector);

            ViewBag.RelativePath = WebConfigurationManager.AppSettings["Video:RelativePath"].Replace("~", "..");

            return View(vm);
        }

        #region Upload

        [HttpGet]
        [Authorize(Roles = "admin, moder")]
        public ActionResult Upload() {
            var userName = User.Identity.GetUserName();
            var hash = Guid.NewGuid().ToString();

            var configPath = WebConfigurationManager.AppSettings["Video:RelativePath"];

            configPath = Path.Combine(Server.MapPath(configPath), "Temp");

            IActionResultProvider<string> result = CreateTemproryDirectory(configPath, userName, hash);

            if (!result.Succeeded)
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError); // or handle somehow error

            var chunkSize = WebConfigurationManager.AppSettings["VideoUpload:chunkSize"];

            #region Filling viewbag

            // ViewBag.TemporaryPath = result.Result; 
            // возможно реализовать если правильно передавать клиенту без потери структуры строки

            ViewBag.UserName = userName;
            ViewBag.Hash = hash;
            ViewBag.ChunkSize = chunkSize;

            #endregion

            return View("UploadVideo");
        }

        #region Partial uploading (server side)

        [HttpPost]
        [Authorize(Roles = "admin, moder")]
        public ActionResult UploadPartial(HttpPostedFileBase chunk, int chunkNumber, string fileName,
            string userName, string hash) {
            try
            {
                var configPath = WebConfigurationManager.AppSettings["Video:RelativePath"];

                var resultedPath = Server.MapPath(configPath);

                var tempFolder = Path.Combine(resultedPath, "Temp", userName, hash);
                // страшно - лучше бы передавать путь

                var path = Path.Combine(tempFolder, chunkNumber + ".tmp");

                chunk.SaveAs(path);
                return Json(new { Succeeded = true });
            }
            catch (Exception e)
            {
                return Json(new { Succeeded = false, Error = e.Message });
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin, moder")]
        public async Task<ActionResult> UploadComplete(string fileName, int chunksCount,
            string userName, string hash) {
            var configPath = WebConfigurationManager.AppSettings["Video:RelativePath"];

            var resultedPath = Server.MapPath(configPath);

            var tempFolder = Path.Combine(resultedPath, "Temp", userName, hash);

            var dest = Path.Combine(resultedPath, fileName);

            for (int i = 0; i < chunksCount; i++)
            {
                try
                {
                    var path = Path.Combine(tempFolder, i.ToString() + ".tmp");
                    Combine(path, dest);
                }
                catch (Exception)
                {
                    Thread.Sleep(5 * 1000);
                    i--;
                    continue;
                }

            }

            Logger.Info("{0}: Загрузка нового видео [{1}]", userName, fileName);

            #region save video info

            try
            {
                string defaultVideoCatName = WebConfigurationManager.AppSettings["defaults:VideoCategory"],
                       ffmpegTemp = Server.MapPath(WebConfigurationManager.AppSettings["ffmpeg:WorkingPath"]);

                VideoCategory videoCat = RepManager.VideoCategories.GetByPredicateSingle(cat => cat.Name == defaultVideoCatName);
                ApplicationUser user = UserManager.FindByName(userName);

                var video = Video.CreateDefault(dest, user, videoCat);

                using (var ffmpegWorker = FFMpegWorker.Create(ffmpegTemp))
                    video.Duration = ffmpegWorker.GetVideoInfo(video.Path).Duration;

                RepManager.Videos.Add(video);
                await RepManager.SaveChangesAsync();
                await Task.Factory.StartNew(() => CleanUp(userName, Guid.Parse(hash)));

                Logger.Info("{0}: Загрузка нового видео [{1}]: Успешно [id: {2}]", userName, fileName, video.Id);

                return Json(new { Success = true, Id = video.Id });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Error = e.Message });
            }

            #endregion
        }

        void Combine(string src, string dest) {
            byte[] bytes = FileIO.ReadAllBytes(src);

            using (var stream = new FileStream(dest, FileMode.Append))
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            FileIO.Delete(src);
        }

        #endregion

        #endregion

        #region Edit

        [HttpGet]
        [Authorize(Roles = "admin, moder")]
        public ActionResult EditVideo(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Video video = RepManager.Videos.GetById(id.Value);

            if (video == null || !FileIO.Exists(video.Path))
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            #region Thumbnails

            var previews = new List<string>();

            var defPreviewsCount = int.Parse(WebConfigurationManager.AppSettings["ffmpeg:PreviewsCount"]);
            var ffmpegTemp = Server.MapPath(WebConfigurationManager.AppSettings["ffmpeg:WorkingPath"]);

            TimeSpan duration;

            using (var ffmpegWorker = FFMpegWorker.Create(ffmpegTemp))
            {
                IEnumerable<VideoThumbnail> thumbnails = ffmpegWorker.GetThumbnails(video.Path, defPreviewsCount);

                duration = ffmpegWorker.GetVideoInfo(video.Path).Duration;

                foreach (VideoThumbnail thumbnail in thumbnails)
                    previews.Add(ImageHelper.GetImageFromFileAsBase64(thumbnail.PathToFile));
            }

            #endregion

            IEnumerable<SelectListItem> cats = GetVideoCatsAsSelectList(video.CategoryId),
                                        users = UsersAsSelectList(video.LectorId);

            VideoEditViewModel vm = Mapper.Map<Video, VideoEditViewModel>(video);

            #region Filling Viewbag

            ViewBag.VideoCats = cats;
            ViewBag.Users = users;
            ViewBag.Previews = previews;

            #endregion

            if (Request.IsAjaxRequest())
                return PartialView("_Edit", vm);

            else
                return View("_Edit", vm);
        }

        [HttpPost]
        [ValidateByAjax]
        [Authorize(Roles = "admin, moder")]
        public async Task<ActionResult> EditVideo(VideoEditViewModel vm) {
            try
            {
                Video video = RepManager.Videos.GetById(vm.Id.Value);

                Logger.Info("{0}: Попытка редактирования видео [{1}]", User.Identity.Name, vm.Name);

                VideoCategory cat = RepManager.VideoCategories.GetById(vm.VideoCategoryId);

                Mapper.Map<VideoEditViewModel, Video>(vm, video);

                video.CategoryId = cat.Id;
                video.IsReady = true;

                if (string.IsNullOrEmpty(video.Path))
                {
                    //var configPath = WebConfigurationManager.AppSettings["Video:RelativePath"];

                    //configPath = Path.Combine(Server.MapPath(configPath), "Temp");
                    video.Path = Server.MapPath(Path.Combine("~/Videos/", vm.FileName));
                }

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Редактирование видео [{1}]: Успешно", User.Identity.Name, vm.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Редактирование видео [{1}]: Неудачно", User.Identity.Name, vm.Name);
                return Json(new { Success = false, Error = e.Message });
            }

        }
        #endregion

        #region Misc

        [HttpPost]
        public void CleanUp(string UserName, Guid Oid) {
            var configPath = WebConfigurationManager.AppSettings["Video:RelativePath"];
            var path = Path.Combine(Server.MapPath(configPath), UserName, Oid.ToString());

            if (Directory.Exists(path))
                Directory.Delete(path, recursive: true);
        }

        [HttpGet]
        public ActionResult GenerateHashName(string extension) {
            var resultedPath = Guid.NewGuid().ToString().Replace("-", "") + "." + extension;
            return Json(new { Name = resultedPath }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}