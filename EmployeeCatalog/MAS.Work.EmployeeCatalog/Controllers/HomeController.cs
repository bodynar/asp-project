﻿namespace MAS.Work.EmployeeCatalog.Controllers
{
    #region References

    using System.Web;
    using System.Web.Mvc;

    #endregion

    public class HomeController : BaseController
    {
        public ActionResult Index()
            => View();

        public ActionResult About()
            => View();

        public ActionResult Contact()
            => View();

        public ActionResult Test()
            => View();
    }
}