﻿namespace MAS.Work.EmployeeCatalog.Controllers
{
    #region References
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using AutoMapper;
    using MAS.Work.EmployeeCatalog.Models;
    using MAS.Work.EmployeeCatalog.Validators;
    using MAS.Work.EmployeeCatalog.ViewModels;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    #endregion

    [Authorize]
    public class AccountController : BaseController
    {
        private ApplicationSignInManager _signInManager;


        private IAuthenticationManager AuthenticationManager
        {
            get {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


        public AccountController() { }

        public AccountController(/*ApplicationUserManager userManager,*/ ApplicationSignInManager signInManager)
        {
            //UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set {
                _signInManager = value;
            }
        }

        #region Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated)
            {
                if (string.IsNullOrEmpty(returnUrl))
                    return RedirectToAction("Index", "Video");
                else
                    return Redirect(returnUrl);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult EasyLogin()
            => PartialView("_Login");

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(AppUserLoginViewModel vm, string returnUrl = "")
        {
            if (string.IsNullOrEmpty(vm.Login) && !string.IsNullOrEmpty(vm.Email))
            {
                ApplicationUser user = await UserManager.FindByEmailAsync(vm.Email);

                if (user == null)
                    return Json(new { Success = false, Error = "Пользователь с такими данными не найден" });

                vm.Login = user.UserName;
            }

            Logger.Info("{0}: Попытка авторизации", vm.Login);

            SignInStatus result = await SignInManager.PasswordSignInAsync(vm.Login, vm.Password, vm.RememberMe, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    Logger.Info("{0}: Успешная авторизация", vm.Login);
                    return Json(new { Success = true, Url = returnUrl });
                case SignInStatus.LockedOut:
                    return Json(new { Success = false, Error = "Пользователь заблокирован." });
                case SignInStatus.RequiresVerification:
                    return Json(new { Success = false, Error = "Треба верiфикацюа." });

                case SignInStatus.Failure:
                default:
                    Logger.Info("{0}: Неудачная авторизация", vm.Login);
                    return Json(new { Success = false, Error = "Неверный пароль" });
            }
        }

        #endregion

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            Logger.Info("{0}: Выход из системы", User.Identity.GetUserName());

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword()
            => PartialView("_ResetPassword");

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(string email)
        {
            Logger.Info("Попытка поиска пользователя с емейлом {0}", email);
            ApplicationUser user = await UserManager.FindByEmailAsync(email);

            if (user == null)
                return Json(new { Success = false, Error = "Пользователь с такими данными не найден" });

            Logger.Info("{0}: Запрос о смене пароля", user.UserName);

            IdentityResult result = await UserManager.ResetPassword(user);

            if (!result.Succeeded)
            {
                Logger.Warn("{0}: Запрос о смене пароля - неудачно. {1}", user.UserName, string.Join("; ", result.Errors));
                return Json(new { Success = false, Errors = result.Errors });
            }

            Logger.Info("{0}: Выслан временный пароль на почту {1}", user.UserName, user.Email);
            return Json(new { Success = true });
        }
    }
}