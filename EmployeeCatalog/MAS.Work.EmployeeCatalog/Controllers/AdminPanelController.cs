﻿namespace MAS.Work.EmployeeCatalog.Controllers {
    #region References

    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using Ef;
    using Helpers;
    using Models;
    using FileIO = System.IO.File;
    using Validators;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using ViewModels;
    using System.Web.Configuration;
    using System.IO;
    #endregion

    [Authorize(Roles = "admin")]
    public class AdminPanelController : BaseController {
        [HttpGet]
        public ActionResult Index() {
            IOrderedEnumerable<ApplicationUser> users = UserManager.Users.ToList().OrderBy(x => x.RegisterDate);
            Logger.Info("{0}: Получение списка пользователей. Найдено {1}.", User.Identity.Name, users.Count());

            return View(users);
        }

        #region User

        #region Create

        [HttpGet]
        public ActionResult UserCreate() {
            IEnumerable<SelectListItem> orgs = GetOrganizationAsSelectList();

            Logger.Info("{0}: Получение списка организаций. Найдено {1}.", User.Identity.Name, orgs.Count());

            IEnumerable<SelectListItem> posts = GetPostsAsSelectList();

            Logger.Info("{0}: Получение списка должностей. Найдено {1}.", User.Identity.Name, posts.Count());

            ViewBag.Orgs = orgs;
            ViewBag.Posts = posts;

            return View();
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> UserCreate(HttpPostedFileBase originFile, AppUserRegisterViewModel vm, string RolesString) {
            try
            {
                var user = new ApplicationUser();

                Logger.Info("{0}: Попытка создать учетную запись пользователя", User.Identity.Name);

                Mapper.Map<AppUserRegisterViewModel, ApplicationUser>(vm, user);

                Employee emp = Mapper.Map<Employee>(vm);

                var password = System.Web.Security.Membership.GeneratePassword(10, 1) + "1";

                Logger.Info("{0}: Запрос временного пароля для учетной записи", User.Identity.Name);

                IdentityResult result = await UserManager.CreateUser(user, password, emp.ManInfo.Initals);

                Logger.Info("{0}: Создание учетной записи пользователя: успешно", User.Identity.Name);

                if (result.Succeeded)
                {
                    #region empCreating

                    Departament dp = RepManager.Departaments.GetById(vm.DepartamentId.Value);
                    Post post = RepManager.Posts.GetById(vm.PostId.Value);

                    emp.Post = post;
                    emp.Departament = dp;
                    emp.UserId = user.Id;

                    if (originFile != null)
                    {
                        byte[] origin = GetByteFromHttpPostedFile(originFile);

                        emp.ManInfo.Photo = origin;
                        emp.ManInfo.PreviewPhoto = ImageHelper.ConvertBase64ToByte(vm.Preview);
                    }

                    RepManager.Employees.Add(emp);

                    Logger.Info("{0}: Попытка создать записи сотрудника для пользователя", User.Identity.Name);

                    await RepManager.SaveChangesAsync();

                    Logger.Info("{0}: Создание записи сотрудника для пользователя: успешно", User.Identity.Name);
                    #endregion

                    await UserManager.AddToRoleAsync(user.Id, "user");

                    return Json(new { Success = result.Succeeded });
                }
                else
                    return Json(new { Success = result.Succeeded, Errors = result.Errors });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка создать учетную запись пользователя", User.Identity.Name);
                return Json(new { Success = false, Errors = new string[] { e.Message } });
            }
        }

        #endregion

        [HttpGet]
        public async Task<ActionResult> Details(string id) {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = await UserManager.FindByIdAsync(id);

            var emp = RepManager.Employees.GetByPredicateSingle(e => e.UserId == id);

            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var vm = new AppUserInfoViewModel();
            Mapper.Map<ManInfo, AppUserInfoViewModel>(emp.ManInfo, vm);
            Mapper.Map<ApplicationUser, AppUserInfoViewModel>(user, vm);

            return PartialView("_UserDetails", vm);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteUser(string id) {
            if (string.IsNullOrEmpty(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Logger.Info("{0}: Попытка поиска учетной записи с id: {1}", User.Identity.Name, id);

            ApplicationUser user = await UserManager.FindByIdAsync(id);

            if (user == null)
            {
                Logger.Info("{0}: Попытка поиска учетной записи с id: {1}: Неудачно. Пользователь не найден", User.Identity.Name, id);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Попытка поиска учетной записи с id: {1}: Успешно. Пользователь найден - ", User.Identity.Name, id,
                user.UserName);

            try
            {
                Logger.Info("{0}: Попытка удалить учетную запись пользователя {1}", User.Identity.Name, user.UserName);

                Employee emp = RepManager.Employees.GetByPredicateSingle(x => x.UserId == id);

                RepManager.ManInfos.Delete(emp.ManInfo);
                RepManager.Employees.Delete(emp);

                await RepManager.SaveChangesAsync();

                IdentityResult result = await UserManager.DeleteUser(user);

                Logger.Info("{0}: Попытка удалить учетную запись пользователя {1}: {2}", User.Identity.Name, user.UserName,
                    result.Succeeded ? "Успешно" : "Неудачно. Причины: " + string.Join("; ", result.Errors));

                return Json(new { Success = result.Succeeded, Errors = result.Errors });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка удалить учетную запись пользователя", User.Identity.Name, user.UserName);
                return Json(new { Success = false, Errors = new string[] { e.Message } });
            }
        }

        #region Edit

        [HttpGet]
        public async Task<ActionResult> EditUser(string id) {
            if (string.IsNullOrEmpty(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Logger.Info("{0}: Попытка поиска учетной записи с id: {1}", User.Identity.Name, id);

            ApplicationUser user = await UserManager.FindByIdAsync(id);

            if (user == null)
            {
                Logger.Info("{0}: Попытка поиска учетной записи с id: {1}: Неудачно. Пользователь не найден", User.Identity.Name, id);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Попытка поиска учетной записи с id: {1}: Успешно. Пользователь найден - ", User.Identity.Name, id,
                user.UserName);

            Employee emp = RepManager.Employees.GetByPredicateSingle(e => e.UserId == id);

            #region ViewBag with info

            ViewBag.Orgs = GetOrganizationAsSelectList(emp.Departament.Organization.Id);
            ViewBag.Deps = GetDepartamentsAsSelectList(emp.Departament.Id, emp.Departament.Organization.Id);
            ViewBag.Posts = GetPostsAsSelectList(emp.Post.Id);

            ViewBag.PreviewImage = ImageHelper.ConvertByteToBase64(emp.ManInfo.PreviewPhoto ??
                                                                GetDefaultPhotoPreview());

            ViewBag.OriginImage = ImageHelper.ConvertByteToBase64(emp.ManInfo.Photo);

            #endregion

            AppUserEditViewModel vm = Mapper.Map<ApplicationUser, AppUserEditViewModel>(user);
            // TODO: TEST 
            Mapper.Map<Employee, AppUserEditViewModel>(emp, vm);

            return View("_UserEdit", vm);
        }

        [HttpPost]
        public async Task<ActionResult> EditUser(AppUserEditViewModel vm, HttpPostedFileBase originFile) {
            if (string.IsNullOrEmpty(vm.UserId))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Logger.Info("{0}: Попытка поиска учетной записи с id: {1}", User.Identity.Name, vm.UserId);

            ApplicationUser user = await UserManager.FindByIdAsync(vm.UserId);

            if (user == null)
            {
                Logger.Info("{0}: Попытка поиска учетной записи с id: {1}: Неудачно. Пользователь не найден", User.Identity.Name, vm.UserId);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Попытка поиска учетной записи с id: {1}: Успешно. Пользователь найден - ", User.Identity.Name, vm.UserId,
                user.UserName);
            try
            {
                Logger.Info("{0}: Попытка изменения данных учетной записи {2} [id: {1}]", User.Identity.Name, vm.UserId, user.UserName);

                Employee emp = RepManager.Employees.GetByPredicateSingle(e => e.UserId == vm.UserId);

                if (vm.UserName.ToLower() != user.UserName.ToLower())
                {
                    await UserManager.SendEmailAsync(vm.UserId, "Смена логина",
                        MailMessageBodyCreator.CreateForLoginChange(emp.ManInfo.Initals, vm.UserName));
                }

                Mapper.Map<AppUserEditViewModel, ApplicationUser>(vm, user);

                UserManager.Update(user);
                Mapper.Map<AppUserEditViewModel, Employee>(vm, emp);

                Departament dep = RepManager.Departaments.GetById(vm.DepartamentId);
                Post post = RepManager.Posts.GetById(vm.PostId);

                emp.Departament = dep;
                emp.Post = post;

                if (originFile != null)
                {
                    byte[] origin = GetByteFromHttpPostedFile(originFile);

                    emp.ManInfo.Photo = origin;
                    emp.ManInfo.PreviewPhoto = ImageHelper.ConvertBase64ToByte(vm.Preview);
                }

                else if (!string.IsNullOrEmpty(vm.Preview))
                {
                    Logger.Info("{0}: Смена аватара профиля пользователю [id: {1}]", emp.User.UserName, vm.UserId);
                    emp.ManInfo.PreviewPhoto = ImageHelper.ConvertBase64ToByte(vm.Preview);
                }

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Попытка изменения данных учетной записи {2} [id: {1}]: Успешно.", User.Identity.Name, vm.UserId, user.UserName);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Попытка поиска учетной записи {2} [id: {1}]: Неудачно.", User.Identity.Name, vm.UserId, user.UserName);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        #endregion

        #region RoleManage

        [HttpGet]
        public ActionResult RoleManage()
            => View();

        [HttpGet]
        public async Task<ActionResult> GetRoles() {
            List<RoleInfoViewModel> vm = new List<RoleInfoViewModel>();

            using (var context = AppDbContext.Create())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

                Logger.Info("{0}: Попытка получения списка ролей.", User.Identity.Name);

                List<IdentityRole> roles = await roleManager.Roles.ToListAsync();

                foreach (IdentityRole role in roles)
                    vm.Add(Mapper.Map<IdentityRole, RoleInfoViewModel>(role));
            }

            Logger.Info("{0}: Получение списка ролей. Найдено {1}.", User.Identity.Name, vm.Count);

            return PartialView("_RoleList", vm);
        }

        [HttpGet]
        public async Task<ActionResult> ShowRoleMembers(string id) {
            if (string.IsNullOrEmpty(id))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            using (var context = AppDbContext.Create())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                IdentityRole role = await roleManager.FindByIdAsync(id);

                Logger.Info("{0}: Попытка поиска роли с id: {1}", User.Identity.Name, id);

                if (role == null)
                {
                    Logger.Info("{0}: Попытка поиска роли с id: {1}: Неудачно. Роль не найдена", User.Identity.Name, id);
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }

                Logger.Info("{0}: Попытка поиска роли с id: {1}: Успешно. Название роли: {2}", User.Identity.Name, id, role.Name);

                var users = new List<ApplicationUser>();

                foreach (IdentityUserRole user in role.Users)
                    users.Add(await UserManager.FindByIdAsync(user.UserId));

                Logger.Info("{0}: Получение списка пользователей роли {1}. Найдено {2}.", User.Identity.Name, role.Name, users.Count);
                return PartialView("_GroupMembers", users);
            }
        }

        [HttpGet]
        public async Task<ActionResult> AddUserToRole() {
            IEnumerable<SelectListItem> roles, users;

            using (var ctx = AppDbContext.Create())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(ctx));

                roles = (await roleManager.Roles.ToListAsync()).Select(x =>
                new SelectListItem() { Text = x.Name, Value = x.Name });

                users = (await UserManager.Users.ToListAsync()).Select(x =>
                new SelectListItem() { Text = x.UserName, Value = x.Id });
            }

            Logger.Info("{0}: Получение списка ролей. Найдено {1}.", User.Identity.Name, roles.Count());
            Logger.Info("{0}: Получение списка пользователей. Найдено {1}.", User.Identity.Name, users.Count());

            ViewBag.Roles = roles;
            ViewBag.Users = users;

            return PartialView("_AddUserToRole");
        }

        [HttpPost]
        public async Task<ActionResult> AddUserToRole(string userId, string role) {
            Logger.Info("{0}: Попытка добавления пользователя [{1}] в группу {2}", User.Identity.Name, userId, role);

            IdentityResult result = await UserManager.AddToRoleAsync(userId, role);

            Logger.Info("{0}: Попытка добавления пользователя [{1}] в группу {2}: {3}", User.Identity.Name, userId, role,
                result.Succeeded ? "Успешно" : "Неудачно. " + string.Join("; ", result.Errors));

            return Json(new { Success = result.Succeeded, Errors = result.Errors });
        }

        [HttpGet]
        public async Task<ActionResult> RemoveUserFromRole() {
            IEnumerable<SelectListItem> roles, users;

            using (var ctx = AppDbContext.Create())
            {
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(ctx));

                users = (await UserManager.Users.ToListAsync()).Select(x =>
                new SelectListItem() { Text = x.UserName, Value = x.Id });

                roles = (await UserManager.GetRolesAsync(users.First().Value)).Select(x =>
                new SelectListItem() { Text = x, Value = x });
            }

            Logger.Info("{0}: Получение списка пользователей. Найдено {1}.", User.Identity.Name, users.Count());

            Logger.Info("{0}: Получение списка ролей для пользователя с [id: {1}]. Найдено {2}.",
                User.Identity.Name, users.First().Value, roles.Count());

            ViewBag.Users = users;
            ViewBag.Roles = roles;

            return PartialView("_RoleRemoveUser");
        }

        [HttpGet]
        public async Task<ActionResult> GetRolesForCurrentUser(string userId) {
            if (string.IsNullOrEmpty(userId))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Logger.Info("{0}: Попытка поиска ролей пользователя с [id: {1}]", User.Identity.Name, userId);

            IEnumerable<SelectListItem> roles = null;

            try
            {
                using (var ctx = AppDbContext.Create())
                {
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(ctx));

                    roles = (await UserManager.GetRolesAsync(userId))
                        .Where(role => role != "user")
                        .Select(x => new SelectListItem() { Text = x, Value = x });
                }

                Logger.Info("{0}: Попытка поиска ролей пользователя с [id: {1}]: Успешно. Найдено ролей {2}.",
                    User.Identity.Name, userId, roles.Count());

                return Json(new { Success = true, Roles = roles }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Попытка поиска ролей пользователя с [id: {1}]: Неудачно", User.Identity.Name, userId);
                return Json(new { Success = false, Error = e.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public async Task<ActionResult> RemoveUserFromRole(string userId, string role) {
            Logger.Info("{0}: Попытка удаления пользователя [{1}] из роли {2}", User.Identity.Name, userId, role);

            IdentityResult result = await UserManager.RemoveFromRoleAsync(userId, role);

            Logger.Info("{0}: Попытка удаления пользователя [{1}] из роли {2}: {3}", User.Identity.Name, userId, role, result.Succeeded ?
                "Успешно" : "Неудачно. " + string.Join("; ", result.Errors));

            return Json(new { Success = result.Succeeded, Errors = result.Errors });
        }

        #endregion

        #region CatalogManage

        [HttpGet]
        public ActionResult CatalogManage()
            => View();

        [HttpGet]
        public ActionResult ShowOrgs(string page, string pageSize) {
            int pageNum = string.IsNullOrEmpty(page) ? 1 : Convert.ToInt32(page);
            int pageSize_ = string.IsNullOrEmpty(pageSize) ? 10 : Convert.ToInt32(pageSize);

            var defaultOrgName = WebConfigurationManager.AppSettings["defaults:Organization"];
            var orgs = RepManager.Organizations.GetByPredicate(org => org.Name != defaultOrgName) // TODO: Организовать на уровне репозитория
                .OrderBy(x => x.Id)
                .Skip((pageNum - 1) * pageSize_)
                .Take(pageSize_)
                .ToList();

            var pageInfo = new PageInfo {
                PageNumber = pageNum,
                PageSize = pageSize_,
                TotalItems = RepManager.Organizations.GetAll().Count()
            };

            var ivm = new PageViewModel<Organization> {
                PageInfo = pageInfo,
                Objects = orgs
            };

            Logger.Info("{0}: Получение списка организаций. Страница {1}\\{2}", User.Identity.Name, pageInfo.PageNumber, pageInfo.TotalPages);

            return PartialView("_OrgList", ivm);
        }

        [HttpGet]
        public ActionResult ShowDeps(string page, string pageSize) {
            int pageNum = string.IsNullOrEmpty(page) ? 1 : Convert.ToInt32(page);
            int pageSize_ = string.IsNullOrEmpty(pageSize) ? 10 : Convert.ToInt32(pageSize);

            var defaultDepName = WebConfigurationManager.AppSettings["defaults:Departament"];
            var deps = RepManager.Departaments.GetByPredicate(dep => dep.Name != defaultDepName) // TODO: Организовать на уровне репозитория
                .OrderBy(x => x.Id)
                .Skip((pageNum - 1) * pageSize_)
                .Take(pageSize_)
                .ToList();

            var pageInfo = new PageInfo {
                PageNumber = pageNum,
                PageSize = pageSize_,
                TotalItems = RepManager.Departaments.GetAll().Count()
            };

            var ivm = new PageViewModel<Departament> {
                PageInfo = pageInfo,
                Objects = deps
            };

            Logger.Info("{0}: Получение списка отделов. Страница {1}\\{2}", User.Identity.Name, pageInfo.PageNumber, pageInfo.TotalPages);

            return PartialView("_DepList", ivm);
        }

        [HttpGet]
        public ActionResult ShowPosts(string page, string pageSize) {
            int pageNum = string.IsNullOrEmpty(page) ? 1 : Convert.ToInt32(page);
            int pageSize_ = string.IsNullOrEmpty(pageSize) ? 10 : Convert.ToInt32(pageSize);

            var defaultPostName = WebConfigurationManager.AppSettings["defaults:Post"];
            var posts = RepManager.Posts.GetByPredicate(post => post.Name != defaultPostName) // TODO: Организовать на уровне репы
                .OrderBy(x => x.Id)
                .Skip((pageNum - 1) * pageSize_)
                .Take(pageSize_)
                .ToList();

            var pageInfo = new PageInfo {
                PageNumber = pageNum,
                PageSize = pageSize_,
                TotalItems = RepManager.Posts.GetAll().Count()
            };

            var ivm = new PageViewModel<PostViewModel> {
                PageInfo = pageInfo,
                Objects = posts.Select((x) => new PostViewModel {
                    Id = x.Id,
                    Name = x.Name,
                    Count = RepManager.Employees.GetByPredicate(emp => emp.Post.Id == x.Id).Count()
                })
            };

            Logger.Info("{0}: Получение списка должностей. Страница {1}\\{2}", User.Identity.Name, pageInfo.PageNumber, pageInfo.TotalPages);

            return PartialView("_PostsList", ivm);
        }

        #region Organization

        #region Create

        [HttpGet]
        public ActionResult CreateOrg()
            => PartialView("_OrgCreate");

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> CreateOrg(Organization org) {
            try
            {
                Logger.Info("{0}: Попытка создания организации с названием {0}", User.Identity.Name, org.Name);
                RepManager.Organizations.Add(org);

                await RepManager.SaveChangesAsync();
                Logger.Info("{0}: Создание организации с названием {0}: успешно", User.Identity.Name, org.Name);
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка создания организации с названием {0}", User.Identity.Name, org.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult EditOrg(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var org = RepManager.Organizations.GetById(id.Value);

            if (org == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var vm = Mapper.Map<Organization, OrganizationViewModel>(org);

            return PartialView("_OrgEdit", vm);
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> EditOrg(OrganizationViewModel vm) {
            var origin = RepManager.Organizations.GetById(vm.Id);

            if (origin == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            try
            {
                Logger.Info("{0}: Попытка изменения данных организации {0}", User.Identity.Name, origin.Name);
                Mapper.Map<OrganizationViewModel, Organization>(vm, origin);

                await RepManager.SaveChangesAsync();
                Logger.Info("{0}: Изменение данных организации {0}: успешно", User.Identity.Name, origin.Name);
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка изменения данных организации {0}", User.Identity.Name, origin.Name);
                return Json(new { Success = false, Error = e.Message });
            }

        }

        #endregion

        [HttpPost]
        public async Task<ActionResult> DeleteOrg(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var org = RepManager.Organizations.GetById(id.Value);

            if (org == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            try
            {
                Logger.Info("{0}: Попытка удаления организации {0}", User.Identity.Name, org.Name);

                var deps = new List<Departament>(org.Departaments);
                foreach (var dep in deps)
                {
                    ApplicationUser user;
                    var emps = new List<Employee>(dep.Employees);
                    foreach (var emp in emps)
                    {
                        user = await UserManager.FindByIdAsync(emp.UserId);

                        RepManager.ManInfos.Delete(emp.ManInfo);
                        RepManager.Employees.Delete(emp);

                        await RepManager.SaveChangesAsync();
                        await UserManager.DeleteAsync(user);
                        Logger.Info("{0}: Попытка удаления учетной записи сотрудника {0}", User.Identity.Name, user.UserName);
                    }
                    RepManager.Departaments.Delete(dep);
                    Logger.Info("{0}: Попытка удаления отдела {0}", User.Identity.Name, dep.Name);
                }

                //RepManager.Departaments.DeleteMultiple(org.Departaments);

                RepManager.Organizations.Delete(org);
                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Удаления организации {0}: Успешно", User.Identity.Name, org.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка удаления организации {0}", User.Identity.Name, org.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        public ActionResult ShowOrgDeps(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var org = RepManager.Organizations.GetById(id.Value);

            if (org == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            Logger.Info("{0}: Получение отделов для организации {1}. Найдено {2} отд.", User.Identity.Name, org.Name, org.Departaments.Count);

            return PartialView("_OrgDeps", org.Departaments);
        }

        #endregion

        #region Departament

        #region Create

        [HttpGet]
        public ActionResult CreateDep() {
            IEnumerable<SelectListItem> orgs = GetOrganizationAsSelectList();

            Logger.Info("{0}: Получение списка организаций для создания отдела. Получено {1} орг.", User.Identity.Name, orgs.Count());
            ViewBag.Orgs = orgs;

            return PartialView("_DepCreate");
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> CreateDep(Departament dep, int? OrgId) {
            try
            {
                var org = RepManager.Organizations.GetById(OrgId.Value);
                Logger.Info("{0}: Попытка создания отдела с названием {1}", User.Identity.Name, dep.Name);

                dep.Organization = org;

                org.Departaments.Add(dep);
                RepManager.Departaments.Add(dep);

                Logger.Info("{0}: Попытка добавления отдела {1} в организацию {2}", User.Identity.Name, dep.Name, org.Name);

                await RepManager.SaveChangesAsync();
                Logger.Info("{0}: Создание отдела с названием {1}: успешно", User.Identity.Name, dep.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка создания отдела с названием {1}", User.Identity.Name, dep.Name);

                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult EditDep(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Departament dep = RepManager.Departaments.GetById(id.Value);

            if (dep == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            IEnumerable<SelectListItem> orgs = GetOrganizationAsSelectList(dep.Organization.Id);

            Logger.Info("{0}: Получение списка организаций для редактирования отдела {1}. Получено {2} орг.", User.Identity.Name, dep.Name, orgs.Count());

            ViewBag.Orgs = orgs;

            var vm = Mapper.Map<Departament, DepartamentViewModel>(dep);

            return PartialView("_DepEdit", vm);
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> EditDep(DepartamentViewModel vm) {
            var origin = RepManager.Departaments.GetById(vm.Id);

            try
            {
                Logger.Info("{0}: Попытка редактирования отдела {1}", User.Identity.Name, origin.Name);

                Mapper.Map<DepartamentViewModel, Departament>(vm, origin);

                if (vm.OrganizationId != origin.Organization.Id)
                {
                    var newOrg = RepManager.Organizations.GetById(vm.OrganizationId);

                    Logger.Info("{0}: Попытка перевода отдела {1} из организации {2} в {3}", User.Identity.Name, origin.Name, origin.Organization.Name, newOrg.Name);

                    //if (newOrg == null)
                    //    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                    origin.Organization.Departaments.Remove(origin);
                    newOrg.Departaments.Add(origin);
                }

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Редактирования отдела {1}: успешно", User.Identity.Name, origin.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка редактирования отдела {1}", User.Identity.Name, origin.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        [HttpGet]
        public ActionResult ShowDepEmps(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var dep = RepManager.Departaments.GetById(id.Value);

            if (dep == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            Logger.Info("{0}: Просмотр сотрудников отдела {1}. Найдено {2} чел.", User.Identity.Name, dep.Name, dep.Employees.Count);
            return PartialView("_DepEmps", dep.Employees);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteDep(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var dep = RepManager.Departaments.GetById(id.Value);

            if (dep == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            try
            {
                Logger.Info("{0}: Попытка удаления отдела {1}", User.Identity.Name, dep.Name);

                ApplicationUser user;
                var emps = new List<Employee>(dep.Employees);

                foreach (var emp in emps)
                {
                    user = await UserManager.FindByIdAsync(emp.UserId);

                    RepManager.ManInfos.Delete(emp.ManInfo.Id);
                    RepManager.Employees.Delete(emp.Id);

                    await RepManager.SaveChangesAsync();
                    await UserManager.DeleteAsync(user);
                    Logger.Info("{0}: Попытка удаления учетной записи пользователя {1}", User.Identity.Name, user.UserName);
                }

                RepManager.Departaments.Delete(dep);

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Удаление отдела {1}: успешно", User.Identity.Name, dep.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка удаления отдела {1}", User.Identity.Name, dep.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        #region Post

        #region Create

        [HttpGet]
        public ActionResult CreatePost()
            => PartialView("_PostCreate");

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> CreatePost(Post post) // maybe PostViewModel?
        {
            try
            {
                Logger.Info("{0}: Попытка удаления должности {1}", User.Identity.Name, post.Name);
                RepManager.Posts.Add(post);

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Удаления должности {1}: успешно", User.Identity.Name, post.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка удаления должности {1}", User.Identity.Name, post.Name);
                return Json(new { Success = false, Error = e.Message });
            }

        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult EditPost(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var post = RepManager.Posts.GetById(id.Value);

            if (post == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var vm = Mapper.Map<Post, PostViewModel>(post);

            return PartialView("_PostEdit", vm);
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> EditPost(PostViewModel vm) {
            var origin = RepManager.Posts.GetById(vm.Id);

            if (origin == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            try
            {
                Logger.Info("{0}: Попытка редактирования должности {1}", User.Identity.Name, origin.Name);

                Mapper.Map<PostViewModel, Post>(vm, origin);

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Изменение данных должности {1}: успешно", User.Identity.Name, origin.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка редактирования должности {1}", User.Identity.Name, origin.Name);
                return Json(new { Success = false, Error = e.Message });
            }

        }

        #endregion

        [HttpPost]
        public async Task<ActionResult> DeletePost(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Post post = RepManager.Posts.GetById(id.Value);

            if (post == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            try
            {
                Logger.Info("{0}: Попытка удаления должности {1}", User.Identity.Name, post.Name);

                var defaultPostName = WebConfigurationManager.AppSettings["defaults:Post"];
                Post def = RepManager.Posts.GetByPredicateSingle(x => x.Name == defaultPostName);

                if (def != null)
                    foreach (Employee emp in post.Employees)
                    {
                        emp.Post = def;
                        Logger.Info("{0}: Изменение должности у сотрудника {1} на \"{2}\"", User.Identity.Name, emp.ManInfo.Initals, def.Name);
                    }

                else
                {
                    ApplicationUser user;
                    var emps = new List<Employee>(post.Employees);
                    foreach (Employee emp in emps)
                    {
                        user = await UserManager.FindByIdAsync(emp.UserId);

                        RepManager.ManInfos.Delete(emp.ManInfo.Id);
                        RepManager.Employees.Delete(emp.Id);

                        await RepManager.SaveChangesAsync();
                        await UserManager.DeleteAsync(user);

                        Logger.Info("{0}: Попытка удаления учетки сотрудника {1}", User.Identity.Name, user.UserName);
                    }
                }

                RepManager.Posts.Delete(post);

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Удаления должности {1}: успешно", User.Identity.Name, post.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка удаления должности {1}", User.Identity.Name, post.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        [HttpGet]
        public ActionResult ShowPostEmps(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var post = RepManager.Posts.GetById(id.Value);

            var emps = RepManager.Employees.GetByPredicate(x => x.Post.Id == id.Value);

            Logger.Info("{0}: Запрос списка видео для категории - {1}. Найдено {2} позиций", User.Identity.Name, post.Name, emps.Count());

            return PartialView("_PostEmps", emps);
        }

        #endregion

        #endregion

        #region VideoCatalog

        [HttpGet]
        public ActionResult VideoCatalog()
            => View();

        [HttpGet]
        public ActionResult ShowVideoCats(string page, string pageSize) {
            int pageNum = string.IsNullOrEmpty(page) ? 1 : Convert.ToInt32(page);
            int pageSize_ = string.IsNullOrEmpty(pageSize) ? 10 : Convert.ToInt32(pageSize);

            var defaultName = WebConfigurationManager.AppSettings["defaults:Post"];

            var objs = RepManager.VideoCategories.GetByPredicate(cat => cat.Name != defaultName) // TODO: Организовать на уровне репы
                .OrderBy(x => x.Id)
                .Skip((pageNum - 1) * pageSize_)
                .Take(pageSize_)
                .ToList();

            var pageInfo = new PageInfo {
                PageNumber = pageNum,
                PageSize = pageSize_,
                TotalItems = RepManager.VideoCategories.GetAll().Count()
            };

            var ivm = new PageViewModel<VideoCategoryInfoViewModel> {
                PageInfo = pageInfo,
                Objects = objs.Select((x) => new VideoCategoryInfoViewModel {
                    Id = x.Id,
                    Name = x.Name,
                    Count = RepManager.Videos.GetByPredicate(vid => vid.CategoryId == x.Id).Count()
                })
            };

            Logger.Info("{0}: Получение списка видео-категорий. Страница {1}\\{2}", User.Identity.Name, pageInfo.PageNumber, pageInfo.TotalPages);

            return PartialView("_VideoCatsList", ivm);
        }

        [HttpGet]
        public ActionResult ShowVideos(string page) {
            int pageNum = string.IsNullOrEmpty(page) ? 1 : Convert.ToInt32(page);
            var pageSize_ = 15;

            var videos = RepManager.Videos.GetAll() // TODO: Организовать на уровне репы
                .OrderBy(x => x.Id)
                .Skip((pageNum - 1) * pageSize_)
                .Take(pageSize_)
                .ToList();

            var pageInfo = new PageInfo {
                PageNumber = pageNum,
                PageSize = pageSize_,
                TotalItems = RepManager.Videos.GetAll().Count()
            };

            var ivm = new PageViewModel<VideoPreviewViewModel> {
                PageInfo = pageInfo,
                Objects = videos.Select((x) => {
                    VideoPreviewViewModel res = Mapper.Map<Video, VideoPreviewViewModel>(x);
                    if (res.Lector != "Не назначен")
                        res.Lector = RepManager.Employees.GetShortName(res.Lector);

                    if (string.IsNullOrEmpty(res.Preview))
                    {
                        var path = WebConfigurationManager.AppSettings["Images:Static:RelativePath"];
                        path = Path.Combine(Server.MapPath(path), "no-preview.png");
                        res.Preview = ImageHelper.GetImageFromFileAsBase64(path);
                    }

                    return res;
                })
                .OrderBy(vid => vid.UploadDate)
            };

            Logger.Info("{0}: Получение списка видео. Страница {1}\\{2}", User.Identity.Name, pageInfo.PageNumber, pageInfo.TotalPages);

            return PartialView("_VideoList", ivm);
        }

        #region Videos

        [HttpPost]
        public async Task<ActionResult> DeleteVideo(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var video = RepManager.Videos.GetById(id.Value);

            if (video == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            try
            {
                Logger.Info("{0}: Попытка удаления видео [{1}]", User.Identity.Name, video.Name);

                Func<string, IActionResultProvider<bool>> delete = (path) => {
                    try
                    {
                        if (FileIO.Exists(path))
                            FileIO.Delete(path);

                        return ResultProvider<bool>.Succeded(true);
                    }
                    catch (Exception e)
                    {
                        return ResultProvider<bool>.Failed(e);
                    }

                };

                var result = delete(video.Path);

                if (!result.Succeeded)
                    return Json(new { Success = false, Error = result.Exc.Message });

                var comments = RepManager.Comments.GetByPredicate(c => c.LectureId == video.Id);

                RepManager.Comments.DeleteMultiple(comments);
                RepManager.Videos.Delete(video.Id);

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Удаление видео [{1}]: успешно", User.Identity.Name, video.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Неудачная попытка удаления видео [{1}]", User.Identity.Name, video.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        #region Video categories

        #region Create

        [HttpGet]
        public ActionResult CreateVideoCat()
            => PartialView("_VideoCatCreate");

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> CreateVideoCat(VideoCategoryCreateViewModel vm) {
            try
            {
                Logger.Info("{0}: Попытка создания видео-категории [{1}]", User.Identity.Name, vm.Name);
                var cat = Mapper.Map<VideoCategoryCreateViewModel, VideoCategory>(vm);

                RepManager.VideoCategories.Add(cat);

                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Создания видео-категории [{1}]", User.Identity.Name, vm.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error("{0}: Неудачная попытка создания видео-категории [{1}]", User.Identity.Name, vm.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult EditVideoCat(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var cat = RepManager.VideoCategories.GetById(id.Value);

            if (cat == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var vm = Mapper.Map<VideoCategory, VideoCategoryEditViewModel>(cat);

            return PartialView("_VideoCatEdit", vm);
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> EditVideoCat(VideoCategoryEditViewModel vm) {
            try
            {
                var videoCat = RepManager.VideoCategories.GetById(vm.Id);
                Mapper.Map<VideoCategoryEditViewModel, VideoCategory>(vm, videoCat);

                await RepManager.SaveChangesAsync();
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Error = e.Message });
            }

        }

        #endregion

        [HttpPost]
        public async Task<ActionResult> DeleteVideoCat(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var cat = RepManager.VideoCategories.GetById(id.Value);

            if (cat == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            try
            {
                Logger.Info("{0}: Попытка удаления видео-категории [{1}]", User.Identity.Name, cat.Name);

                var videos = RepManager.Videos.GetByPredicate(vid => vid.CategoryId == id.Value);

                var defaultVideoCat = WebConfigurationManager.AppSettings["defaults:VideoCategory"];

                var def = RepManager.VideoCategories.GetByPredicateSingle(vcat => vcat.Name == defaultVideoCat);

                if (def == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                foreach (var video in videos)
                    video.CategoryId = def.Id;

                RepManager.VideoCategories.Delete(cat);
                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Удаление видео-категории [{1}]: успешно", User.Identity.Name, cat.Name);

                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Info(e, "{0}: Неудачная попытка удаление видео-категории [{1}]", User.Identity.Name, cat.Name);
                return Json(new { Success = false, Error = e.Message });
            }
        }

        [HttpGet]
        public ActionResult ShowVideoCatsVideos(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var cat = RepManager.VideoCategories.GetById(id.Value);

            var vids = RepManager.Videos.GetByPredicate(x => x.CategoryId == id.Value);

            Logger.Info("{0}: Запрос списка видео для категории - {1}. Найдено {2} видео.", User.Identity.Name, cat.Name, vids.Count());

            return PartialView("_VideoCatsVideos", vids);
        }

        #endregion

        #endregion
    }
}