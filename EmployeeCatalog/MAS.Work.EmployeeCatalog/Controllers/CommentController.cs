﻿namespace MAS.Work.EmployeeCatalog.Controllers {
    #region References
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using MAS.Work.EmployeeCatalog.Helpers;
    using MAS.Work.EmployeeCatalog.Models;
    using MAS.Work.EmployeeCatalog.ViewModels;
    using Microsoft.AspNet.Identity;
    #endregion
    public class CommentController : BaseController {

        [HttpGet]
        public ActionResult GetComments(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Logger.Info("{0}: Попытка поиска видео с [id: {1}].", User.Identity.GetUserName(), id.Value);
            Video video = RepManager.Videos.GetById(id.Value);

            if (video == null)
            {
                Logger.Info("{0}: Попытка поиска видео с [id: {1}]: Неудачно.", User.Identity.GetUserName(), id.Value);
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Поиск комментариев для видео [id: {1}]", User.Identity.GetUserName(), id.Value);

            IEnumerable<Comment> comments = RepManager.Comments.GetByPredicate(c => c.LectureId == id.Value);
            Logger.Info("{0}: Поиск комментариев для видео [id: {1}]: Найдено {2} шт.",
                User.Identity.GetUserName(), id.Value, comments.Count());

            var vms = new List<CommentPreviewViewModel>();

            foreach (Comment comment in comments)
            {
                CommentPreviewViewModel vm = Mapper.Map<Comment, CommentPreviewViewModel>(comment);

                vm.Author = RepManager.Employees.GetShortNameByUserId(comment.AuthorId);

                Employee emp = RepManager.Employees.GetByUserId(comment.AuthorId);
                vm.AuthorAvatar = ImageHelper.ConvertByteToBase64(emp.ManInfo.PreviewPhoto ?? GetDefaultPhotoPreview());

                vms.Add(vm);
            }
            Logger.Info(@"{0}: Проведение мэппинга комментариев для просмотра: {1}\{2}",
                User.Identity.GetUserName(), vms.Count, comments.Count());

            return PartialView("_Comments", vms);
        }

        #region Add

        [HttpGet]
        public ActionResult AddComment(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Logger.Info("{0}: Попытка поиска видео с [id: {1}].", User.Identity.GetUserName(), id.Value);
            Video video = RepManager.Videos.GetById(id.Value);

            if (video == null)
            {
                Logger.Info("{0}: Попытка поиска видео с [id: {1}]: Неудачно.", User.Identity.GetUserName(), id.Value);
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Попытка поиска видео с [id: {1}]: Найдено 1 вхождение: {2}",
                User.Identity.GetUserName(), id.Value, video.Name);

            var vm = new CommentAddViewModel() { LectureId = id.Value };

            return PartialView("_AddComment", vm);
        }

        [HttpPost]
        public async Task<ActionResult> AddComment(CommentAddViewModel vm) {
            Logger.Info("{0}: Попытка добавления комментария.", User.Identity.GetUserName());
            try
            {
                Comment comment = Mapper.Map<CommentAddViewModel, Comment>(vm);

                var userId = User.Identity.GetUserId();

                comment.Lecture = RepManager.Videos.GetById(vm.LectureId);
                comment.AuthorId = userId;

                RepManager.Comments.Add(comment);

                await RepManager.SaveChangesAsync();
                Logger.Info("{0}: Попытка добавления комментария: Успешно [id: {1}]", User.Identity.GetUserName(), comment.Id);
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Попытка добавления комментария: Неудачно", User.Identity.GetUserName());
                return Json(new { Success = false, Error = e.Message });
            }
        }

        #endregion

        [HttpPost]
        public async Task<ActionResult> Delete(int? id) {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Comment comment = RepManager.Comments.GetById(id.Value);

            Logger.Info("{0}: Попытка поиска комментария с [id: {1}]", User.Identity.Name, id.Value);

            if (comment == null)
            {
                Logger.Info("{0}: Попытка поиска комментария с [id: {1}]: Неудачно, комментарий не найден", User.Identity.Name, id.Value);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Попытка поиска комментария с [id: {1}]: Успешно", User.Identity.Name, id.Value);

            try
            {
                Logger.Info("{0}: Попытка удаления комментария с [id: {1}]", User.Identity.Name, id.Value);

                RepManager.Comments.Delete(comment);
                await RepManager.SaveChangesAsync();

                Logger.Info("{0}: Попытка поиска комментария с [id: {1}]: Успешно", User.Identity.Name, id.Value);
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Попытка удаления комментария с [id: {1}]: Неудачно", User.Identity.Name, id.Value);
                return Json(new { Success = false, Error = "Произошла ошибка удаления" });
            }
        }
    }
}