﻿namespace MAS.Work.EmployeeCatalog.Controllers
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Security;
    using Ef;
    using Helpers;
    using MAS.Work.EmployeeCatalog.Repositories;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Models;
    #endregion

    public class BaseController : Controller
    {
        private RepManager rm_;

        protected NLog.Logger Logger = NLog.LogManager.GetLogger("Controllers logger");

        public RepManager RepManager 
            => rm_ ?? (rm_ = RepManager.Create(new AppDbContext()));

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        #region Get Departaments depends on selected Organization

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetDepsAsync(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return Json(GetDepartamentsAsSelectList(orgId: id.Value), JsonRequestBehavior.AllowGet); // TODO: TEST
        }

        #endregion

        #region Get T model as select list

        protected IEnumerable<SelectListItem> GetVideoCatsAsSelectList(int? id = null)
        {
            var @default = WebConfigurationManager.AppSettings["defaults:VideoCategory"];

            Logger.Info("{0}: Получение списка категорий видео для отображения без дефолтного [{1}]", User.Identity.Name, @default);

            return RepManager.VideoCategories.GetByPredicate(c => c.Name != @default)
                  .Select(cat => new SelectListItem()
                  {
                      Text = cat.Name,
                      Value = cat.Id.ToString(),
                      Selected = id.HasValue ? cat.Id == id.Value : false,
                  });
        }

        protected IEnumerable<SelectListItem> GetOrganizationAsSelectList(int? id = null)
        {
            var @default = WebConfigurationManager.AppSettings["defaults:Organization"];

            Logger.Info("{0}: Получение списка организаций для отображения без дефолтной [{1}]", User.Identity.Name, @default);

            return RepManager.Organizations.GetByPredicate(c => c.Name != @default)
                  .Select(cat => new SelectListItem()
                  {
                      Text = cat.Name,
                      Value = cat.Id.ToString(),
                      Selected = id.HasValue ? cat.Id == id.Value : false,
                  });
        }

        protected IEnumerable<SelectListItem> GetPostsAsSelectList(int? id = null)
        {
            var @default = WebConfigurationManager.AppSettings["defaults:Post"];

            Logger.Info("{0}: Получение списка должностей для отображения без дефолтной [{1}]", User.Identity.Name, @default);

            return RepManager.Posts.GetByPredicate(c => c.Name != @default)
                  .Select(cat => new SelectListItem()
                  {
                      Text = cat.Name,
                      Value = cat.Id.ToString(),
                      Selected = id.HasValue ? cat.Id == id.Value : false,
                  });
        }

        protected IEnumerable<SelectListItem> GetDepartamentsAsSelectList(int? selectedId = null, int? orgId = null)
        {
            var @default = WebConfigurationManager.AppSettings["defaults:Departament"];

            Logger.Info("{0}: Получение списка отделов для отображения без дефолтного [{1}]", User.Identity.Name, @default);

            return RepManager.Departaments.GetByPredicate(c => c.Name != @default 
            && orgId.HasValue ? c.Organization.Id == orgId : true)
                  .Select(cat => new SelectListItem()
                  {
                      Text = cat.Name,
                      Value = cat.Id.ToString(),
                      Selected = selectedId.HasValue ? cat.Id == selectedId.Value : false,
                  });
        }

        #endregion

        #region Get Users to represent as select list
        protected IEnumerable<SelectListItem> UsersAsSelectList(string selectedId = "")
            => UserManager.Users.AsEnumerable().Select(user => new SelectListItem()
            {
                Text = user.UserName,
                Value = user.Id,
                Selected = user.Id == selectedId
            });

        #endregion

        #region Cropper

        // запрос из вьюхи для ресайза изображения
        [HttpPost]
        public ActionResult ResizeImage(HttpPostedFileBase file)
        {
            Logger.Info("{0}: Запрос об изменении размеров изображения [{1}]", User.Identity.Name, file.FileName);
            using (Stream stream = file.InputStream)
            {
                return Json(new { Success = true, resizedImage = GetResizedImageInBase64(stream) });
            }
        }

        string GetResizedImageInBase64(Stream stream)
        {
            var img = new WebImage(stream);

            if (img.Height > 2000 || img.Width > 2000)
            {
                var ratio = img.Height / (double)img.Width;

                img.Resize(1500, (int)(1500 * ratio));
            }

            var bytes = img.GetBytes();

            return ImageHelper.ConvertByteToBase64(bytes);
        }

        /// <summary>
        /// Ресайзинг оригинального фото для отображения в представлении (чаще редактировании)
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        protected string GetResizedImageInBase64ForView(byte[] buffer)
        {
            using (var stream = new MemoryStream(buffer))
            {
                return GetResizedImageInBase64(stream);
            }
        }

        /// <summary>
        /// Преобразования типа данных оригинального фото в набор байтов (чаще для дальнейшего сохранения)
        /// </summary>
        /// <param name="httpFile"></param>
        /// <returns></returns>
        protected byte[] GetByteFromHttpPostedFile(HttpPostedFileBase httpFile)
        {
            using (var stream = httpFile.InputStream)
            {
                var img = new WebImage(stream);

                return img.GetBytes();
            }
        }

        [HttpPost]
        public ActionResult CropThePhoto(string origin,
            string cropX, string cropY,
           string cropWidth, string cropHeight,
           string angle)
        {
            try
            {
                double
                x = ParseToDouble(cropX),
                y = ParseToDouble(cropY),
                width = ParseToDouble(cropWidth),
                height = ParseToDouble(cropHeight),
                rotateAngle = ParseToDouble(angle);

                byte[] miniature;

                byte[] originBytes = ImageHelper.ConvertBase64ToByte(origin);

                Logger.Info("{0}: Запрос кроппинга изображения до размеров [{1} x {2}]", 
                            User.Identity.Name, cropHeight, cropHeight);

                miniature = ImageHelper.CropImage(originBytes,
                        (int)Math.Round(x), (int)Math.Round(y),
                        (int)Math.Round(width), (int)Math.Round(height), rotateAngle);


                return Json(new { Success = true, preview = ImageHelper.ConvertByteToBase64(miniature) });
            }
            catch (Exception e)
            {
                return Json(new { Success = false, Error = e.Message });
            }


        }

        protected double ParseToDouble(string value)
        {
            double result = Double.NaN;
            value = value.Trim();
            if (!double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("ru-RU"), out result))
            {
                if (!double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("en-US"), out result))
                {
                    return Double.NaN;
                }
            }
            return result;
        }

        #endregion

        #region Misc

        protected byte[] GetDefaultPhotoPreview()
        {
            var path = WebConfigurationManager.AppSettings["Images:Static:RelativePath"];
            path = Path.Combine(Server.MapPath(path), "no-avatar.jpg");
            return System.IO.File.ReadAllBytes(path);
        }

        protected IActionResultProvider<string> CreateTemproryDirectory(string originDirectory, params string[] Oids)
        {
            var resultedPath = originDirectory;

            foreach (var pathId in Oids)
                resultedPath = Path.Combine(resultedPath, pathId);

            Logger.Info("{0}: Попытка создания временной директории [path: {1}]", User.Identity.Name, resultedPath);
            try
            {
                Directory.CreateDirectory(resultedPath);
                Logger.Info("{0}: Попытка создания временной директории [path: {1}]: Успешно", User.Identity.Name, resultedPath);
                return ResultProvider<string>.Succeded(resultedPath);
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Попытка создания временной директории [path: {1}]: Неудачно", User.Identity.Name, resultedPath);
                return ResultProvider<string>.Failed(e);
            }
        }

        protected IActionResultProvider<bool> DeleteTemporaryFolder(string temporaryFolder)
        {
            Logger.Info("{0}: Попытка удаления временной директории [path: {1}]", User.Identity.Name, temporaryFolder);
            try
            {
                Directory.Delete(temporaryFolder);
                Logger.Info("{0}: Попытка удаления временной директории [path: {1}]: Успешно", User.Identity.Name, temporaryFolder);
                return ResultProvider<bool>.Succeded(true);
            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Попытка удаления временной директории [path: {1}]: Неудачно", User.Identity.Name, temporaryFolder);
                return ResultProvider<bool>.Failed(e);
            }
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _userManager?.Dispose();
                _userManager = null;

                rm_?.Dispose();
                rm_ = null;

                base.Dispose(disposing);
            }

        }
    }
}