﻿namespace MAS.Work.EmployeeCatalog.Controllers {
    #region References

    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using MAS.Work.EmployeeCatalog.Helpers;
    using Microsoft.AspNet.Identity;
    using Models;
    using Validators;
    using ViewModels;

    #endregion

    [Authorize]
    public class AccountManageController : BaseController {
        public ActionResult Index()
            => RedirectToAction("AccountManage");

        [HttpGet]
        public async Task<ActionResult> AccountManage() {
            var id = User.Identity.GetUserId();

            Logger.Info("{0}: Попытка поиска учетной записи с [id: {1}]", User.Identity.Name, id);

            ApplicationUser user = await UserManager.FindByIdAsync(id);

            if (user == null)
            {
                Logger.Info("{0}: Попытка поиска учетной записи с [id: {1}]: Неудачно", User.Identity.Name, id);
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Попытка поиска учетной записи с [id: {1}]: Успешно, найден {2}", User.Identity.Name, id, user.UserName);

            AppUserManageViewModel vm = Mapper.Map<ApplicationUser, AppUserManageViewModel>(user);

            return View(vm);
        }

        #region Change password

        [HttpGet]
        public ActionResult ChangePassword() {
            var vm = new AppUserChangePasswordViewModel() {
                UserId = User.Identity.GetUserId()
            };

            ViewBag.UserName = User.Identity.Name;

            return View("_ChangePassword", vm);
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> ChangePassword(AppUserChangePasswordViewModel vm) {
            ApplicationUser user = await UserManager.FindByIdAsync(vm.UserId);

            try
            {
                IdentityResult result = await UserManager.ChangePasswordAsync(vm.UserId, vm.OldPassword, vm.NewPassword);

                Logger.Info("{0}: Попытка смена пароля в личном кабинете", user.UserName);

                if (!result.Succeeded)
                {
                    Logger.Info("{0}: Смена пароля в личном кабинете: Неудачно [{1}]", user.UserName, string.Join("; ", result.Errors));

                    return Json(new { Success = false, Errors = result.Errors });
                }
                Logger.Info("{0}: Смена пароля в личном кабинете", user.UserName);

                user.HasTemporaryPassword = false;

                await UserManager.UpdateAsync(user);

                return Json(new { Success = true });

            }
            catch (Exception e)
            {
                Logger.Error(e, "{0}: Смена пароля в личном кабинете: Неудачно", user.UserName);

                return Json(new { Success = true, Errors = new string[] { e.Message } });
            }
        }

        #endregion

        #region Change info

        [HttpGet]
        public ActionResult ChangeInfo() {
            var id = User.Identity.GetUserId();

            Logger.Info("{0}: Поиск сотрудника с [UserId: {1}]", User.Identity.Name, id);
            Employee emp = RepManager.Employees.GetByPredicateSingle(em => em.UserId == id);

            if (emp == null)
            {
                Logger.Info("{0}: Поиск сотрудника с [UserId: {1}]: Неудачно.", User.Identity.Name, id);
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            Logger.Info("{0}: Поиск сотрудника с [UserId: {1}]: Успешно, найден {2}", User.Identity.Name, id, emp.ManInfo.Initals);

            #region ViewBag with info

            ViewBag.PreviewImage = ImageHelper.ConvertByteToBase64(emp.ManInfo.PreviewPhoto ?? GetDefaultPhotoPreview());

            if (emp.ManInfo.Photo != null)
                ViewBag.OriginImage = GetResizedImageInBase64ForView(emp.ManInfo.Photo);

            #endregion

            EmployeeViewModel vm = Mapper.Map<Employee, EmployeeViewModel>(emp);

            return View("_ChangeInfo", vm);
        }

        [HttpPost]
        [ValidateByAjax]
        public async Task<ActionResult> ChangeInfo(HttpPostedFileBase originFile, EmployeeViewModel vm) {

            Logger.Info("{0}: Поиск сотрудника с [id: {1}]", User.Identity.Name, vm.Id.Value);

            Employee emp = RepManager.Employees.GetById(vm.Id.Value);

            if (emp == null)
            {
                Logger.Info("{0}: Поиск сотрудника с [id: {1}]: Неудачно", User.Identity.Name, vm.Id.Value);
                return Json(new { Success = false, Error = "Учетная запись сотрудника не найдена" });
            }

            Logger.Info("{0}: Поиск сотрудника с [id: {1}]: Успешно, найден ", User.Identity.Name, vm.Id.Value, emp.ManInfo.Initals);

            //ManInfo mi = RepManager.ManInfos.GetById(emp.ManInfo.Id);

            Mapper.Map<EmployeeViewModel, ManInfo>(vm, emp.ManInfo);

            if (originFile != null)
            {
                byte[] origin = GetByteFromHttpPostedFile(originFile);

                Logger.Info("{0}: Смена фото профиля", emp.User.UserName);

                emp.ManInfo.Photo = origin;
                emp.ManInfo.PreviewPhoto = ImageHelper.ConvertBase64ToByte(vm.Preview);
            }

            else if (!string.IsNullOrEmpty(vm.Preview))
            {
                Logger.Info("{0}: Смена аватара профиля", emp.User.UserName);
                emp.ManInfo.PreviewPhoto = ImageHelper.ConvertBase64ToByte(vm.Preview);
            }

            await RepManager.SaveChangesAsync();

            Logger.Info("{0}: Изменение персональных данных: Успешно", emp.User.UserName);

            return Json(new { Success = true });
        }

        #endregion
    }
}