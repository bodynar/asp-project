﻿namespace MAS.Work.EmployeeCatalog.Controllers
{
    #region References

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.Mvc;
    using AutoMapper;
    using MAS.Work.EmployeeCatalog.Helpers;
    using MAS.Work.EmployeeCatalog.Models;
    using MAS.Work.EmployeeCatalog.Validators;
    using Microsoft.AspNet.Identity;
    using ViewModels;

    #endregion

    [Authorize]
    public class EmployeesController : BaseController
    {
        #region Edit

        [Authorize(Roles = "admin, moder")]
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Employee emp = RepManager.Employees.GetById(id.Value);

            if (emp == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            var workOrgId = emp.Departament.Organization.Id;

            #region ViewBag with work info

            ViewBag.Orgs = GetOrganizationAsSelectList(workOrgId);
            //    RepManager.Organizations.GetAll().Select(x => new SelectListItem()
            //{
            //    Text = x.Name,
            //    Value = x.Id.ToString(),
            //    Selected = x.Id == workOrgId
            //});

            ViewBag.Posts = GetPostsAsSelectList(emp.Post.Id);
            //RepManager.Posts.GetAll()
            //.Select(x => new SelectListItem()
            //{
            //    Text = x.Name,
            //    Value = x.Id.ToString(),
            //    Selected = x.Id == emp.Post.Id
            //});

            ViewBag.Deps = GetDepartamentsAsSelectList(emp.Departament.Id, emp.Departament.Organization.Id);
                // RepManager.Departaments.GetByPredicate(x => x.Organization.Id == emp.Departament.Organization.Id)
                //.Select(x => new SelectListItem()
                //{
                //    Text = x.Name,
                //    Value = x.Id.ToString(),
                //    Selected = x.Id == emp.Departament.Id
                //});

            #endregion

            EmployeeViewModel vm = Mapper.Map<EmployeeViewModel>(emp);

            ViewBag.PreviewImage = ImageHelper.ConvertByteToBase64(emp.ManInfo.PreviewPhoto ?? GetDefaultPhotoPreview());

            if (emp.ManInfo.Photo != null)
                ViewBag.OriginImage = GetResizedImageInBase64ForView(emp.ManInfo.Photo);

            Logger.Info("Запрос редактирования сотрудника {0}", emp.ManInfo.Initals);

            return View(vm);
        }

        [Authorize(Roles = "admin, moder")]
        [HttpPost]
        [ValidateByAjax]
        public async Task<JsonResult> Edit(HttpPostedFileBase originFile, EmployeeViewModel vm)
        {
            Employee emp = RepManager.Employees.GetById(vm.Id.Value);

            ManInfo mi = RepManager.ManInfos.GetById(emp.ManInfo.Id);

            Mapper.Map<EmployeeViewModel, ManInfo>(vm, mi);

            Departament newDep = RepManager.Departaments.GetById(vm.DepartamentId.Value);
            Post newPost = RepManager.Posts.GetById(vm.PostId.Value);

            emp.Departament = newDep;
            emp.Post = newPost;

            if (originFile != null)
            {
                byte[] origin = GetByteFromHttpPostedFile(originFile);

                Logger.Info("Изменение фото сотрудника {0}", emp.ManInfo.Initals);

                emp.ManInfo.Photo = origin;
                emp.ManInfo.PreviewPhoto = ImageHelper.ConvertBase64ToByte(vm.Preview);
            }

            else if (!string.IsNullOrEmpty(vm.Preview)){ 
                emp.ManInfo.PreviewPhoto = ImageHelper.ConvertBase64ToByte(vm.Preview);
                Logger.Info("Изменение аватара сотрудника {0}", emp.ManInfo.Initals);
            }

            await RepManager.SaveChangesAsync();

            Logger.Info("Изменение данных сотрудника {0}", emp.ManInfo.Initals);

            return Json(new { Success = true });
        }

        #endregion

        [HttpGet]
        public ActionResult Index()
            => View();

        [HttpGet]
        public ActionResult Details(int? id)
        {
            Employee employee = RepManager.Employees.GetById(id.Value);

            ViewBag.ImageData = ImageHelper.ConvertByteToBase64(employee.ManInfo.PreviewPhoto ?? 
                                                                GetDefaultPhotoPreview());

            return PartialView("_Details", employee);
        }

        [HttpGet]
        public ActionResult ShowEmployees(string page, string pageSize)
        {
            int pageNum = string.IsNullOrEmpty(page) ? 1 : Convert.ToInt32(page);
            int pageSize_ = string.IsNullOrEmpty(pageSize) ? 10 : Convert.ToInt32(pageSize);

            IEnumerable<Employee> emps = RepManager.Employees.GetAll() // TODO: Organize in Rep
                .OrderBy(x => x.Id)
                .Skip((pageNum - 1) * pageSize_)
                .Take(pageSize_);

            var pageInfo = new PageInfo
            {
                PageNumber = pageNum,
                PageSize = pageSize_,
                TotalItems = RepManager.Employees.GetAll().Count()
            };

            var ivm = new PageViewModel<Employee>
            {
                PageInfo = pageInfo,
                Objects = emps
            };

            Logger.Info("{0}: Получение списка сотрудников. Страница {1}\\{2}", User.Identity.Name, pageInfo.PageNumber, pageInfo.TotalPages);

            return PartialView("_PList", ivm);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (!id.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Employee employee = RepManager.Employees.GetById(id.Value);

            if (employee == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            ApplicationUser user = await UserManager.FindByIdAsync(employee.UserId);

            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            RepManager.ManInfos.Delete(employee.ManInfo);
            RepManager.Employees.Delete(employee);

            await RepManager.SaveChangesAsync();

            IdentityResult res = await UserManager.DeleteAsync(user);

            if (!res.Succeeded)
                return Json(new { Success = false, Erorrs = res.Errors });

            Logger.Info("{0}: Удаление сотрудника {1} [учетка {2}]", User.Identity.Name, employee.ManInfo.Initals, user.UserName);

            return Json(new { Success = true });
        }

        #region Filter

        [HttpGet]
        public ActionResult SearchEmployees()
        {
            ViewBag.Posts = GetPostsAsSelectList(); // TODO: Add logging

            return PartialView("_Search");
        }

        [HttpGet]
        public ActionResult Filter(string name, int? postId, string age, string startDate, string endDate)
        {
            if (!postId.HasValue)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Post post = RepManager.Posts.GetById(postId.Value);

            DateTime dtStart = string.IsNullOrEmpty(startDate) ? DateTime.MinValue : DateTime.Parse(startDate);
            DateTime dtEnd = string.IsNullOrEmpty(endDate) ? DateTime.MaxValue : DateTime.Parse(endDate);

            IEnumerable<Employee> emps = RepManager.Employees.GetByPredicate(emp =>
           emp.ManInfo.SecondName.ToLower().Contains(name.ToLower()) // А правильно ли это?
                       && emp.ManInfo.BirthDay > dtStart
                       && emp.ManInfo.BirthDay < dtEnd
                       && emp.Post.Id == postId.Value
            );
           
            return PartialView("_EmpList", emps.Where(emp => emp.ManInfo.Age.ToString().Contains(age)));
        }

        #endregion
    }
}
