﻿namespace MAS.Work.EmployeeCatalog.Ef
{
    #region References
    using System.Data.Entity;
    using Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Web.Configuration;
    #endregion
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }

        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Departament> Departaments { get; set; }
        public virtual DbSet<Organization> Organizations { get; set; }
        public virtual DbSet<ManInfo> ManInfos { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<Video> Videos { get; set; }
        public virtual DbSet<VideoCategory> VideoCategorys { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
    }

    public class AppDbInitializer : CreateDatabaseIfNotExists<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            CreateRole("admin", roleManager);

            CreateRole("moder", roleManager);

            CreateRole("user", roleManager);

            if (userManager.FindByName("bodynar") == null)
                CreateAdmin(userManager);

            #region Default
            string 
                defaultVideoCat = WebConfigurationManager.AppSettings["defaults:VideoCategory"],
                defaultPost = WebConfigurationManager.AppSettings["defaults:Post"],
                defaultOrganization = WebConfigurationManager.AppSettings["defaults:Organization"],
                defaultDepartament = WebConfigurationManager.AppSettings["defaults:Departament"];

            var defOrg = new Organization() { Name = defaultOrganization };
            var defDep = new Departament() { Name = defaultDepartament, Organization = defOrg };
            var defPost = new Post() { Name = defaultPost };
            var defVideoCat = new VideoCategory() { Name = defaultVideoCat };

            context.Organizations.Add(defOrg);
            context.Departaments.Add(defDep);
            context.Posts.Add(defPost);
            context.VideoCategorys.Add(defVideoCat);

            context.SaveChanges();

            #endregion

            base.Seed(context);
        }

        void CreateAdmin(ApplicationUserManager userManager)
        {
            var user = new ApplicationUser
            {
                Email = "example@example.com",
                UserName = "bodynar",
                HasTemporaryPassword = true,
            };

            var result = userManager.Create(user, "9438587");
            if (result.Succeeded)
                userManager.AddToRole(user.Id, "admin");
        }

        void CreateRole(string roleName, RoleManager<IdentityRole> roleManager)
        {
            var role = new IdentityRole { Name = roleName };
            roleManager.Create(role);
        }
    }
}